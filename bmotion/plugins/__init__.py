import copy
import importlib
import random
import re
from typing import Union, List

from discord import TextChannel
from discord.ext.commands import Bot, Cog

from helpers.conf import Config


class BaseCog(Cog):
    plugin_type = None

    def __init__(self, bot):
        self.bot = bot

    @Cog.listener()
    async def on_ready(self, *args, **kwargs):
        plugins_config = Config.get('enabled_plugins', {})
        plugins = plugins_config.get(self.plugin_type)
        if not plugins:
            print(f"Not loading any {self.plugin_type} plugins")
            return

        for plugin in plugins:
            try:
                mod = importlib.import_module(plugin)
            except ImportError as e:
                print(f"Error loading {plugin}. ({str(e)}) Skipping..")
            else:
                init_fn = getattr(mod, 'init', None)
                if callable(init_fn):
                    init_fn(self.bot)


class Plugin:
    _bot: Bot = None

    _simple_plugins = {}
    _complex_plugins = {}
    _output_plugins = {}

    @classmethod
    def set_bot(cls, bot: Bot):
        cls._bot = bot

    @classmethod
    def add_simple(cls, name: str, regexp: Union[List[str], str], chance: int, responses: list):
        if name in cls._simple_plugins or name in cls._complex_plugins:
            print(f"Plugin with name {name} already loaded!")
            return

        print(f"Loading simple plugin: simple.{name}...")
        cls._simple_plugins[name] = {
            'name': name,
            'regexp': regexp,
            'chance': chance,
            'responses': responses,
        }

    @classmethod
    def add_complex(cls,
                    name: str,
                    regexp: Union[List[str], str],
                    chance: int,
                    callback: callable,
                    regexp_method: str = 'search'):
        if name in cls._simple_plugins or name in cls._complex_plugins:
            print(f"Plugin with name {name} already loaded!")
            return

        print(f"Loading complex plugin: complex.{name}...")
        cls._complex_plugins[name] = {
            'name': name,
            'regexp': regexp,
            'regexp_method': regexp_method,
            'chance': chance,
            'callback': callback,
        }

    @classmethod
    def add_output(cls, name: str, klass):
        if name in cls._output_plugins:
            print(f"Output plugin with name {name} already loaded!")

        print(f"Loading output plugin: output.{name}...")
        cls._output_plugins[name] = {
            'name': name,
            'class': klass,
        }

    @staticmethod
    def test_regexp(regexp, text, method=None):
        method = method or 'search'
        if isinstance(regexp, list):
            for r in regexp:
                matches = Plugin.test_regexp(r, text)
                if matches:
                    return matches
            else:
                return None

        _regexp = regexp
        # check for tags
        bot_nicks = copy.copy(Config.get('alternate_names', []))
        bot_nicks.append(r'the bots?')
        _regexp = _regexp.replace('%bot%', fr"(\<@!{Plugin._bot.user.id}\>|{'|'.join(bot_nicks)})")

        re_fn = getattr(re, method)
        matches = re_fn(_regexp, text, re.IGNORECASE)
        return matches

    @classmethod
    def find_simple(cls, text):
        for plugin_key in sorted(cls._simple_plugins.keys()):
            plugin = cls._simple_plugins[plugin_key]
            if Plugin.test_regexp(plugin['regexp'], text, plugin.get('regexp_method')) and Plugin.do_chance_roll(plugin['chance']):
                return plugin

    @classmethod
    def find_complex(cls, text):
        for plugin_key in sorted(cls._complex_plugins.keys()):
            plugin = cls._complex_plugins[plugin_key]
            if (matches := Plugin.test_regexp(plugin['regexp'], text, plugin.get('regexp_method'))) and Plugin.do_chance_roll(plugin['chance']):
                yield matches, plugin

    @staticmethod
    def do_chance_roll(chance):
        return chance >= random.randint(0, 100)

    @classmethod
    def flip_a_coin(cls):
        return random.choice([True, False])

    @classmethod
    async def process_output(cls, text, channel: TextChannel):
        _text = text
        while re.search(r'%([A-Za-z]+|=)({.*})?', _text):
            # keep iterating the outputs list until we can no longer match the output plugin syntax
            for plugin_key in sorted(cls._output_plugins.keys()):
                klass = cls._output_plugins[plugin_key]['class']
                inst = klass(bot=cls._bot, channel=channel)
                _text = await inst.process_response(_text)

        return _text
