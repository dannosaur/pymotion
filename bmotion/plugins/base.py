import random
import re

from discord import Message


class BasePlugin:
    klass = ''
    name = ''
    # the chance (out of 100) that the plugin will fire
    chance = 0

    # a regex string (or list of regex strings) to apply to the message
    match = None

    @property
    def plugin_id(self):
        return f'{self.klass}:{self.name}'

    async def send_message(self, message):
        print(f"plugin {self.plugin_id} received message: {message}")
        if self.test_matches(message) and self.should_run():
            await self.callback(message)

    def test_matches(self, message: Message):
        if self.match is None:
            return
        elif isinstance(self.match, (list, tuple)):
            for match in self.match:
                if re.match(match, message.content):
                    print("message matches!")
                    return True
        else:
            if re.match(self.match, message.content):
                print("message matches!")
                return True

        return False

    def should_run(self):
        return random.randint(0, 100) < self.chance

    async def callback(self, message: Message):
        raise NotImplementedError

    def init(self):
        print(f"Initializing {self.plugin_id}..")

    def format_message_for_response(self, response: str, message: Message):
        _response = response
        if _response.startswith('/'):
            _response = f'_{_response[1:]}_'

        _response = _response.format(
            name=message.author.name,
            user_tag=f"<@{message.author.id}>",
        )

        return _response
