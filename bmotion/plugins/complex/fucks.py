from discord import Message

from bmotion.modules.abstracts import register_abstract
from bmotion.modules.friendship import drift_friendship, like
from bmotion.modules.mood import get_happy, get_horny, get_unlonely, frightened
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.conf import Config
from helpers.messaging import send_message


async def fuck_callback(message: Message, matches):
    is_kinky = Config.get('kinky', False)
    if matches.group('kinky') and not is_kinky:
        drift_friendship(message.author, -5)
        await frightened(message.channel, message.author)
    else:
        if like(message.author):
            await send_message("%VAR{rarrs}", message.channel, message.author)
            drift_friendship(message.author, 4)
            get_happy(multiplier=2)
            get_horny()
            get_unlonely()
        else:
            await send_message("%VAR{fuck_fail}", message.channel, message.author)
            drift_friendship(message.author, -10)

    return STOP_PROCESSING


Plugin.add_complex(
    name='fucks',
    regexp=r'^_(?P<kinky>ass|arse|bottom|anal|rape(s)?|fist)? ?((s(e|3)x(o|0)r(s|z|5))|(fluffles)|bofs|fucks|shags|fondles|ravages|rapes|spanks|kisses|zoent|snogs|touches|fingers) %bot%',
    chance=100,
    callback=fuck_callback,
)
register_abstract(
    name='fuck_fail',
    lst=[
        "get off me",
        "wtf",
        "not without dinner and drinks first you don't",
        "/calls the police",
        "/phones the police",
        "not again %SMILEY{sad}",
    ]
)
