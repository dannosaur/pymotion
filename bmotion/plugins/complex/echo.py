from discord.ext import commands
from discord.ext.commands import Bot

from helpers.messaging import send_message, owner_only


@commands.command(name='echo')
@owner_only()
async def echo(ctx, *args):
    await send_message(args[0], ctx.message.channel, ctx.message.author, use_timers=False)


def init(bot: Bot):
    bot.add_command(echo)
