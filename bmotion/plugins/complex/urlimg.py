# Plugin.add_simple(name='url-img', regexp=r'(?:http\:|https\:)?\/\/.*\.(?:png|jpg|jpeg|gif)', chance=25, responses=ABSTRACTS.get('rarrs'))
import requests

from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message


async def urlimg(message, matches):

    url = matches.group(0)
    response = requests.head(url)
    if response.status_code == 404:
        await send_message("Aw, it's broken :(", message.channel, message.author)
    else:
        if Plugin.do_chance_roll(25):
            await send_message("%VAR{rarrs}", message.channel, message.author)
    return STOP_PROCESSING


Plugin.add_complex(
    name='url-img',
    regexp=r'(?:http\:|https\:)?\/\/.*\.(?:png|jpg|jpeg|gif)',
    chance=100,
    callback=urlimg,
)
