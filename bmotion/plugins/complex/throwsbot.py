from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message


async def throwsbot(message, matches):
    verb = matches.group('verb')
    target = matches.group('target') or message.author.mention

    if isinstance(target, str):
        if target == Plugin.test_regexp(r'%bot%', target):
            # thrown at ourselves?
            await send_message("hmm", message.channel)
            return STOP_PROCESSING

    await send_message("%VAR{thrownAts}", message.channel, message.author, interpolation_args=[target])
    return STOP_PROCESSING


Plugin.add_complex(
    name='throwsbot',
    regexp=r'^_(?P<verb>throws|chucks|lobs|fires|launches|ejects|pushes) %bot%( (at|to|though|out of|out|off|into) (?P<target>[^_]*))?',
    chance=100,
    callback=throwsbot,
)
