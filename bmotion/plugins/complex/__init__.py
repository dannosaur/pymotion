from pprint import pprint
from re import Match

from discord import Message
from discord.ext.commands import Cog

from bmotion.modules import system
from bmotion.plugins import Plugin, BaseCog

STOP_PROCESSING = object()


class ComplexPluginCog(BaseCog):
    plugin_type = 'complex'

    @Cog.listener()
    async def on_message(self, message: Message):
        if message.author.id == self.bot.user.id:
            return

        if message.author.id in system.IGNORES:
            return

        matches: Match
        for matches, plugin in Plugin.find_complex(message.content):
            print(f"Executing complex plugin: {plugin['name']}")
            callback: callable = plugin['callback']
            returned = await callback(message, matches)
            if returned == STOP_PROCESSING:
                break
