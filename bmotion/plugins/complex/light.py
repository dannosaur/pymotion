import asyncio

from discord import Message

from bmotion.modules.abstracts import register_abstract
from bmotion.modules.friendship import drift_friendship
from bmotion.modules.mood import get_unlonely
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message


async def fire1_callback(message: Message, matches):
    with message.channel.typing():
        await send_message("%VAR{burns}", message.channel, message.author)

    get_unlonely()
    drift_friendship(message.author, -1)
    return STOP_PROCESSING


Plugin.add_complex(
    name='fire1',
    regexp=[
        r'^_(lights?|sets fire to) %bot%_',
        r'^_sets %bot% (alight|on fire)_',
    ],
    chance=100,
    callback=fire1_callback,
)

register_abstract(
    name='burns',
    lst=[
        "/burns",  # %|%bot[50,%VAR{extinguishes}]",
        "*flames*",  # %|%bot[50,%VAR{extinguishes}]",
        "B%REPEAT{2:5:O}M",
        "b%REPEAT{2:5:o}m",
        "BLAM",
        "pop",
        "/goes up in flames",  # %|%bot[50,%VAR{extinguishes}]",
        "*smoulder*",  # %|%bot[50,%VAR{extinguishes}]",
        "/explodes",
        "huk",
        "%colen",
        "/torches",  # %|%bot[50,%VAR{extinguishes}]",
        "/informs the world 'It's time to burn'",
    ]
)
register_abstract(
    name='extinguishes',
    lst=[
        "/puts %% out",
        "/pours water on %%",
        "/wraps {user_tag} in a fire blanket",
        "/laughs",  # %|%BOT[%ABSTRACT{unsmiles}]",
        # "%ABSTRACT{shocked}",
        "kaBLAM",
        "taunt",
    ]
)
