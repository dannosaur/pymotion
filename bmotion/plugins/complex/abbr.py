from discord.ext import commands
from discord.ext.commands import Bot

from bmotion.modules.abstracts import register_abstract, get_abstract
from helpers.messaging import send_message


@commands.command(name='abbr')
async def abbr(ctx, *args):
    if len(args) > 0 and args[0] in ['adult', 'xxx']:
        noun1 = get_abstract('abbr_adult_adj')
        noun2 = get_abstract('abbr_adult_nouns')
        verb = get_abstract('abbr_adult_verbs')
    else:
        noun1 = get_abstract('abbr_adj')
        noun2 = get_abstract('abbr_nouns')
        verb = get_abstract('abbr_verbs')

    acronym = (noun1[0] + verb[0] + noun2[0]).upper()
    await send_message(f"{acronym}: {noun1}-{verb}-{noun2}", ctx.message.channel)


def init(bot: Bot):
    bot.add_command(abbr)


register_abstract(
    name='abbr_nouns',
    lst=[
        "mullet", "cheese", "individual", "cellular", "cup", "monitor", "desk", "teddy", "shirt", "phaser", "klingon",
        "shoe", "black", "crunch", "celeb", "bounce", "grape", "spit", "hole", "gravel", "dung", "heap", "sheep",
        "crash", "screen", "crisps", "sword", "maple", "fish", "hip-hop", "wesley", "toilet",
    ]
)
register_abstract(
    name='abbr_adj',
    lst=[
        "hot", "cold", "purple", "clean", "freezing", "thooper", "white", "starchy", "bavarian", "woolly", "blippy",
        "decent", "smart", "coloured", "flavoured", "norwegian", "swede", "brit", "dutchman", "american", "canadian",
        "german",
    ]
)
register_abstract(
    name='abbr_verbs',
    lst=[
        "wielding", "powered", "enabled", "spinning", "cycling", "phasing", "sounding", "clapping", "shoving",
        "plowing", "screwed", "thinking", "holding", "shooting", "warping", "beaming", "tasting", "smelling",
    ]
)
register_abstract(
    name='abbr_adult_nouns_t',
    lst=[
        "sex", "vibrator", "rubber", "lace", "package", "knickers", "dildo", "vibrator", "sex", "nipple", "bum",
        "stockings", "rabbit",
    ]
)
register_abstract(
    name='abbr_adult_adj_t',
    lst=[
        "thexy", "horny", "hot", "moist", "wet", "lubricated",
    ]
)
register_abstract(
    name='abbr_adult_verbs_t',
    lst=[
        "licking", "moaning", "licking", "pumping", "grinding", "rubbing", "ooh-ahhing", "screwing", "oomphing",
    ]
)
register_abstract(
    name='abbr_adult_nouns',
    lst=[
        "%VAR{abbr_nouns}",
        "%VAR{abbr_adult_nouns_t}",
    ]
)
register_abstract(
    name='abbr_adult_verbs',
    lst=[
        "%VAR{abbr_verbs}",
        "%VAR{abbr_adult_verbs_t}",
    ]
)
register_abstract(
    name='abbr_adult_adj',
    lst=[
        "%VAR{abbr_adj}",
        "%VAR{abbr_adult_adj_t}",
    ]
)
