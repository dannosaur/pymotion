import logging
import os
import random
import re

import nltk
import requests
from discord import Message
from discord.ext.commands import Bot
from nltk.tokenize import word_tokenize

from bmotion.modules.abstracts import register_abstract
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message

logger = logging.getLogger(__name__)

PLUGIN_INIT = {}


def bot() -> Bot:
    return PLUGIN_INIT['bot']


def immolation_tokenation(s):
    def is_joining_tag(_tag):
        return _tag in [',', 'CC']

    tags = nltk.pos_tag(word_tokenize(s))
    # print(tags)

    items = []
    item = ""
    skip_count = 0
    for i, (word, tag) in enumerate(tags):
        if skip_count > 0:
            skip_count -= 1
            continue

        mention_variation_1 = ''.join([w for w, _ in tags[i:i + 4]])
        mention_variation_2 = ''.join([w for w, _ in tags[i:i + 5]])

        discord_mention = re.compile(r'<@!?(\d)+>')
        if discord_mention.match(mention_variation_1):
            # print("variant 1, skipping 3")
            item = mention_variation_1
            skip_count = 3
            continue
        elif discord_mention.match(mention_variation_2):
            # print("variant 2, skipping 4")
            item = mention_variation_2
            skip_count = 4
            continue
        else:
            # print(f'word: {word} - tag: {tag} - items: {items} - item: {item}')
            if is_joining_tag(tag) and item:
                # print("appending item")
                items.append(item)
                item = ""
            elif not is_joining_tag(tag):
                if not item:
                    # print("not item")
                    item = word
                elif tag == 'POS':
                    # print("POS")
                    item += word
                else:
                    # print("else")
                    item += ' ' + word

    if item:
        items.append(item)

    # print(f"items: {items}")
    return items


async def attack_callback(message: Message, matches):
    target = matches.group('target').strip()
    damage = random.randint(1, 1500)
    params = {
        'target': target,
        'object': matches.group('object').strip(),
        'damage_score': damage,
    }
    if sub_matches := re.match(r'(an?|the|some|his|her) (?P<object>.+)', params['object']):
        params['object'] = sub_matches.group('object')

    attack_response = 'attack_responses_lose'
    if Plugin.flip_a_coin():
        attack_response = 'attack_responses_gain'

    if Plugin.do_chance_roll(10):
        attack_response = 'attack_responses_miss'

    await send_message(
        "%%VAR{%(response_type)s}" % {'response_type': attack_response},
        message.channel,
        message.author,
        params=params,
        use_timers=False,
    )

    if attack_response == 'attack_responses_lose':
        await poke_woobot_api(message.author, message.channel, target, damage)
    elif attack_response == 'attack_responses_gain':
        damage *= -1
        await poke_woobot_api(message.author, message.channel, target, damage)

    return STOP_PROCESSING


async def immolate_callback(message: Message, matches):
    targets = immolation_tokenation(matches.group('targets'))
    targets = [m.strip() for m in targets if m is not None]
    d_targets = set(targets)

    for target in d_targets:
        score = random.randint(1, 1500)
        await send_immolation_message(message.channel, message.author, score, target)

    if len(targets) > len(d_targets):
        score = random.randint(1, 1500)
        await send_immolation_message(message.channel, Plugin._bot.user, score, f"<@{message.author.id}>")
    return STOP_PROCESSING


async def send_immolation_message(channel, author, score, target):
    params = {
        'target': target,
        'score': score,
    }
    if 1 <= score <= 500:
        await send_message(
            random.choice([
                "{target} is lightly charred by {user_tag}'s flames. {target} takes {score} damage.",
                "{target} is mildly singed and takes {score} damage from {user_tag}'s fire.",
                # "https://media.giphy.com/media/it3siiqqn7vxe/giphy.gif",
            ]),
            channel,
            author,
            params=params,
            use_timers=False
        )
    elif 500 < score <= 1000:
        await send_message(
            random.choice([
                "{target} takes some intense heat from {user_tag}'s fire and loses {score} HP!",
                "{user_tag} throws {target} onto the BBQ, sets the temperature to HOT HOT HOT, and roasts {target} for {score} minutes.",
                # "https://media.giphy.com/media/26tP6YeGmxkHqcKc0/giphy.gif",
            ]),
            channel,
            author,
            params=params,
            use_timers=False,
        )
    elif 1000 < score <= 1500:
        await send_message(
            random.choice([
                "A raging fire! {target} becomes pure carbon and takes {score} damage from {user_tag}'s intense heat! What was that? Pentaborane?!",
                "{user_tag} breaks out the flamethrower, sets it to its highest setting, and delivers {score} damage to {target}.",
                # "https://media.giphy.com/media/5nsiFjdgylfK3csZ5T/giphy.gif",
            ]),
            channel,
            author,
            params=params,
            use_timers=False,
        )

    await poke_woobot_api(author, channel, target, score)


async def poke_woobot_api(attacker, channel, target, damage):
    if not os.environ.get('WOOBOT_ENABLED', 'no') == 'yes':
        return

    target_key = 'TargetItem'
    if matches := re.match(r'^<@[^\d]*(?P<user_id>\d+)>$', target):
        target_key = 'TargetDiscordUserId'
        target = int(matches.group('user_id'))

    base_url = os.environ.get('WOOBOT_BASE_URL')
    url = f'{base_url}/projects/woobot2/api/attacks'
    payload = {
        'AttackerDiscordUserId': attacker.id,
        'DiscordChannelId': channel.id,
        target_key: target,
        'Damage': damage,
    }
    logger.info(f"url = {url}")
    logger.info(f"payload = {payload}")

    response = requests.post(
        url=url,
        json=payload,
        headers={
            'x-api-key': os.environ.get('WOOBOT_API_KEY'),
        }
    )
    logger.info(f"response[{response.status_code}] = {response.content}")


def init(bot: Bot):
    PLUGIN_INIT['bot'] = bot


"""
this does weird things...

_sets @Woo482#0336 on fire_ _sets @Woo482#0336 on fire_ _sets @Woo482#0336 on fire_
"""
Plugin.add_complex(
    name='immolate',
    # regexp=r'(?:_sets )?(\w+[^, (and)]+)(?: on fire.*_)?',
    # regexp=r'(?:_sets )?([\w<@>]+[^, (and)]+)(?: on fire.*_)?',
    regexp=r'^_sets (?P<targets>.*) on fire(.*)?_$',
    # regexp_method='findall',
    chance=100,
    callback=immolate_callback,
)

Plugin.add_complex(
    name='attacks',
    regexp=r'^_attacks (?P<target>.+) (with|using) (?P<object>.+)_$',
    chance=100,
    callback=attack_callback,
)

# register_abstract(
#     name='attack_responses',
#     lst=[
#         "%VAR{attack_responses_lose}",
#         "%VAR{attack_responses_gain}",
#         "%VAR{attack_responses_miss}",
#     ]
# )

register_abstract(
    name='attack_responses_lose',
    lst=[
        "{user_tag} attacks {target} with '{object}' for {damage_score} damage.",
        "{target} takes {damage_score} damage from {user_tag}'s '{object}'",
        "{target} is tremendously damaged by the {object} and takes {damage_score} damage!",
        "It's super effective! {target} takes {damage_score} damage!",
        "It's not very effective... {target} takes {damage_score} damage!",
    ]
)

register_abstract(
    name='attack_responses_gain',
    lst=[
        "{target} absorbs the damage and gains {damage_score} HP!",
        "{target} dodges the attack, and gains {damage_score} HP for being so sneaky!",
    ]
)

register_abstract(
    name='attack_responses_miss',
    lst=[
        "MISS!",
        "{target} is immune to '{object}'",
        "It doesn't affect {target}",
    ]
)
