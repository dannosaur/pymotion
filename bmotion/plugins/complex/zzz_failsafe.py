import re

from discord import Message

from bmotion.modules.abstracts import register_abstract, get_abstract
from bmotion.modules.friendship import drift_friendship
from bmotion.modules.mood import get_happy, get_horny
from bmotion.plugins import Plugin
from helpers.messaging import send_message


async def failsafe_callback(message: Message, matches):
    """
  regexp {^([^ ]+) ((across|near|at|with|to|against|from|over|under|in|on|next to) )?} $text matches verb dir
  if {$verb == ""} {
    return 2
  }
    """
    verb = None
    dir = None
    if sub_matches := re.match(r'^_(?P<verb>[^ ]+) ((?P<dir>across|near|at|with|to|against|from|over|under|in|on|next to) )?', message.content):
        verb = sub_matches.group('verb')
        dir = sub_matches.group('dir')
        if verb == '':
            return

    if sub_matches := re.match(r'(?P<verb>giggl|hug(gle)?|p[ae]t|rub|like|<3|sniff|smell|nibbl|tickl)e?s?', verb):
        verb = sub_matches.group('verb')
        await send_message(get_abstract('failsafe_nice'), message.channel, message.author, params={'verb': verb})
        get_happy()
        drift_friendship(message.author, 1)
        return True

    if re.match(r'(squashes|squishes|squee+zes)', verb):
        await send_message(get_abstract('squeezeds'), message.channel, message.author)
        get_happy()
        drift_friendship(message.author, 1)
        return True

    if re.match(r'(dance|sing|play|bop|doof)s?', verb):
        await send_message(get_abstract('failsafe_niceactions'), message.channel, message.author)
        get_happy()
        drift_friendship(message.author, 10)
        return True

    if re.match(r'(cum|come|jizze|spurt)s?', verb):
        await send_message(get_abstract('failsafe_wtfs'), message.channel, message.author)
        get_happy()
        get_horny()
        drift_friendship(message.author, 10)
        return True

    if re.match(r'(look|stare|eye|frown)s?', verb):
        await send_message(get_abstract('failsafe_lookback'), message.channel, message.author)
        get_happy()
        drift_friendship(message.author, 10)
        return True



Plugin.add_complex(
    name='zzz-failsafe',
    regexp=r'^_(.+?)s?( at|with)? %bot%',
    chance=100,
    callback=failsafe_callback,
)

register_abstract(
    name='failsafe_nice',
    lst=[
        "mmm",
        "%SMILEY{smile}",
        ("%SMILEY{smile}", "/gives {user_tag} %VAR{sillythings}"),
        "i do love a good {verb}ing",
    ]
)
register_abstract(
    name='squeezeds',
    lst=[
        "/pops",
        "/bursts",
        ("/is compressed to a singularity and sucks in all of spacetime", "whoops"),
        "/deflates",
        "%SMILEY{smile}",
    ]
)
register_abstract(
    name='failsafe_niceactions',
    lst=[
        "wh%REPEAT{3:7:e} %SMILEY{smile}",
        "%SMILEY{smile}",
        "/bounces around",
        "_drool_",
    ]
)
register_abstract(
    name='failsafe_wtfs',
    lst=[
        "%VAR{satons}",
        "%VAR{shocked}",
    ]
)
register_abstract(
    name='failsafe_lookback',
    lst=[
        "/stares at {user_tag}",
        "hello, yes?",
        "i can still see you...",
        "/poses",
        "/bounces away",
        "%SMILEY{smile}",
        "/looks at {user_tag}",
    ]
)