import logging
import random
import re
from typing import Union

from discord import Message, Member
from discord.ext import commands
from discord.ext.commands import Bot

from bmotion.plugins import Plugin
from helpers.messaging import send_message, owner_only
from helpers.redis import RedisClient

logger = logging.getLogger('plugins.complex.facts')
PLUGIN_INIT = {}


class FactsNotFound(Exception):
    pass


def bot() -> Bot:
    return PLUGIN_INIT['bot']


def redis() -> RedisClient:
    return PLUGIN_INIT['redis']


async def fact_callback(message: Message, matches):
    logger.debug("callback: start")
    fact = matches.group('fact').strip()
    item = matches.group('item').strip().lower()

    member: Member = message.author
    nick = member.display_name
    logger.debug(f"callback: nick = {nick}")

    if re.search(r'.*answer was.*', message.content):
        logger.debug("callback: ignoring trivia bots")
        return

    if re.search(r'(Fail|Win) count is now', message.content):
        logger.debug("callback: ignoring fail/win counters")
        return

    if message.content.endswith('?'):
        logger.debug("callback: ignoring questions")
        return

    if 3 > len(fact) > 30:
        logger.debug("callback: fact too short")
        return

    if re.search(r'\b(like|what|which|have|it|that|when|where|there|then|this|who|you|yours|why|he|she)\b', item):
        logger.debug(f"callback: item was {item}, ignoring")
        return

    if item == 'i':
        logger.debug(f"callback: item is 'i', re-assigning to {nick}")
        item = nick

    fact = re.sub(r'\bme\b', nick, fact)
    fact = fact.lower().strip()
    fact = re.sub(r'\bmy\b', '%OWNER{' + nick + '}', fact)

    if item_matches := re.search(r'<@!?(?P<user_id>\d+)>', item):
        logger.debug(f"callback: user id = {item_matches.group('user_id')}")
        item = item_matches.group('user_id')

    fact_key = f'what:{item}'
    facts = redis().get(fact_key, [])
    if fact not in facts:
        logger.debug(f"callback: Learning fact: {item} == {fact}")
        facts.append(fact)
        redis().set(fact_key, facts)


def get_fact(item):
    fact_key = f'what:{item}'
    facts = redis().get(fact_key, [])
    if not facts:
        raise FactsNotFound

    return random.choice(facts)


@commands.command('clearfacts')
@owner_only()
async def clear_facts(ctx, thing: Union[Member, str]):
    original_thing = thing
    if isinstance(thing, Member):
        thing = thing.id
        original_thing = original_thing.mention

    fact_key = f'what:{thing}'
    redis().delete(fact_key)
    await ctx.send(f"Deleted all facts about {original_thing}")


@commands.command('learnfact')
@owner_only()
async def learn_facts(ctx, thing: Union[Member, str], fact: str, *args):
    if len(args) > 0:
        await send_message("Enclose facts in \"quotes\".", ctx.message.channel, use_timers=False)
        return

    original_thing = thing
    if isinstance(thing, Member):
        thing = thing.id
        original_thing = original_thing.mention

    fact_key = f'what:{thing}'
    facts = redis().get(fact_key, [])
    if fact not in facts:
        logger.debug(f"callback: Learning fact: {thing} == {fact}")
        facts.append(fact)
        redis().set(fact_key, facts)
        await send_message(f"I've now learned that fact about {original_thing}", ctx.message.channel, use_timers=False)
    else:
        await send_message(f"I already know this about {original_thing}", ctx.message.channel, use_timers=False)


@commands.command('facts')
@owner_only()
async def get_facts(ctx, *args):
    try:
        item = args[0]
    except IndexError:
        await send_message("What do you want me to get facts for?", ctx.message.channel, use_timers=False)
    else:
        original_item = item
        if item_matches := re.match(r'<@!?(?P<user_id>\d+)>', item):
            item = item_matches.group('user_id')

        facts = redis().get(f'what:{item.lower()}')
        if not facts:
            await send_message(f"I have no facts for {original_item}", ctx.message.channel, use_timers=False)
        else:
            await send_message(f"I have the following facts for {original_item}: {', '.join(facts)}", ctx.message.channel, use_timers=False)


Plugin.add_complex(
    name='facts',
    regexp=r'(?P<item>[^,.]*) (is|was|=|am) (?P<fact>.*)',
    chance=100,
    callback=fact_callback,
)


def init(bot: Bot):
    PLUGIN_INIT['bot'] = bot
    PLUGIN_INIT['redis'] = RedisClient(prefix='plugin:complex:facts')

    bot.add_command(get_facts)
    bot.add_command(learn_facts)
    bot.add_command(clear_facts)
