from discord import Message

from bmotion.modules.mood import get_horny, get_happy
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message

async def blbl_callback(message: Message, matches):
    get_horny()
    get_happy()
    # if {![bMotion_interbot_me_next $channel]} { return 0 }
    await send_message("%VAR{rarrs}", message.channel, message.author)
    # bMotionDoAction $channel [bMotionGetRealName $nick $host] "%VAR{rarrs}"
    return STOP_PROCESSING

Plugin.add_complex(
    name='blbl',
    regexp=r'bl{2,}',
    chance=50,
    callback=blbl_callback,
)