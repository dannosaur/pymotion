from bmotion.modules.abstracts import add_to_abstract
from bmotion.plugins import Plugin


async def verb_catch(message, matches):
    stem = matches.group('stem')

    if stem != '':
        if len(stem) < 3:
            return

        if stem.endswith('e'):
            stem = stem[0:-1]

        add_to_abstract('sillyverbs', stem)


Plugin.add_complex(
    name='aaa-verbcatch',
    regexp=r'\b((?P<stem>\w+)s)\b',
    chance=100,
    callback=verb_catch
)
