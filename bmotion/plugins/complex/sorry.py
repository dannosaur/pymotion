from bmotion.modules import friendship, mood
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message


async def sorry(message, matches):
    await send_message("%VAR{sorryoks} %%", message.channel, message.author)
    mood.get_happy()
    mood.get_unlonely()
    friendship.drift_friendship(message.author, 3)
    return STOP_PROCESSING

Plugin.add_complex(
    name='sorry1',
    regexp=r"(i'm)?( )?(very)?( )?sorry(,)? %bot%",
    chance=100,
    callback=sorry,
)
Plugin.add_complex(
    name='sorry2',
    regexp=r"%bot%:? sorry",
    chance=100,
    callback=sorry,
)
