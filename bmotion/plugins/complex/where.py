import asyncio

from discord import Message

from bmotion.modules.friendship import get_friendship
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.human import sleep_proportional_to_response
from helpers.messaging import send_message


async def callback(message: Message, matches):
    with message.channel.typing():
        friendship = get_friendship(message.author)
        if friendship > 40:
            response = "%VAR{answer_wheres}"
        else:
            response = "%VAR{upyourbums}"
        await asyncio.sleep(sleep_proportional_to_response(response))
        await send_message(response, message.channel, message.author)

    return STOP_PROCESSING


Plugin.add_complex(
    name='where',
    regexp=[
        r'^%bot% ?,? ?where',
        r'^where (.*)%bot%\?'
    ],
    chance=100,
    callback=callback,
)
