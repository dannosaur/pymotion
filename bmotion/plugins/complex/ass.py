import re

from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message


async def ass_callback(message, matches):
    first = matches.group('first')
    second = matches.group('second')

    ass = 'ass'
    if first.isupper() and second.isupper():
        ass = ass.upper()

    if 'so' in second:
        return STOP_PROCESSING

    if re.search(r'^(s)$', first):
        return STOP_PROCESSING

    await send_message(f'{first} {ass}-{second}', message.channel)


Plugin.add_complex(
    name='ass',
    regexp=r'(?P<first>[a-z]+)[- ]ass (?P<second>[a-z]+)',
    chance=85,
    callback=ass_callback,
)
