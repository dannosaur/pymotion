from discord import Message

from bmotion.modules.abstracts import register_abstract
from bmotion.plugins import Plugin
from helpers.messaging import send_message


async def correct_callback(message: Message, matches):
    await send_message("%VAR{shouldhaves}", message.channel)


Plugin.add_complex(
    name='correct-of',
    regexp=r'(must|should) of',
    chance=30,
    callback=correct_callback,
)

register_abstract(
    name='shouldhaves',
    lst=[
        "\"{user_tag} have\" %VAR{smiles}",
        "{user_tag} what?",
        "{user_tag} HAVE, {user_tag} HAVE",
        "`s/of/have/`",
    ]
)
