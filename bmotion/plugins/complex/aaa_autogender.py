from bmotion.modules.userinfo import get_user_gender, UNKNOWN, set_user_gender, MALE, FEMALE
from bmotion.plugins import Plugin


async def autolearn_gender(message, matches):
    pronoun = matches.group('pronoun').strip()
    gender = get_user_gender(message.author)
    if gender == UNKNOWN:
        if pronoun.lower() == 'his':
            print(f"Learning that {message.author} is male")
            set_user_gender(message.author, MALE)

        elif pronoun.lower() == 'her':
            print(f"Learning that {message.author} is female")
            set_user_gender(message.author, FEMALE)


Plugin.add_complex(
    name='aaa-autogender',
    regexp=r'[a-z]+s (?P<pronoun>his|her) ',
    chance=100,
    callback=autolearn_gender,
)