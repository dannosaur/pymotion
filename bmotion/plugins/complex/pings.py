from bmotion.modules.abstracts import register_abstract
from bmotion.plugins import Plugin
from helpers.messaging import send_message


async def pings(message, matches):
    await send_message("%VAR{pings}", message.channel, message.author)


Plugin.add_complex(
    name='pings',
    regexp=r'^_(ping|pong)s? %bot%',
    chance=100,
    callback=pings,
)

register_abstract(
    name='pings',
    lst=[
        "/pings %% in the %VAR{bodypart}",
        "/pongs%|/sprays some deodorant",
        "/pongs",
        "oi that tickles %%",
        "%VAR{sound}",
        "%VAR{goAways}",
    ]
)
