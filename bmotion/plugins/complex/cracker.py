import datetime
import random

from discord.ext.commands import Bot

from bmotion.modules.abstracts import register_abstract
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.messaging import send_message
from helpers.redis import RedisClient

REDIS_PREFIX = 'plugin:complex:cracker'

PLUGIN_INIT = {}


def redis() -> RedisClient:
    return PLUGIN_INIT['redis']


async def cracker(message, matches):
    # current_month = datetime.datetime.now().month
    # if current_month != 12:
    #     return STOP_PROCESSING

    await send_message("%VAR{cracker_boom}", message.channel, message.author)
    hats = redis().get('hats', 0)

    if random.randint(0, 3) == 0:
        # i won
        await send_message("%VAR{cracker_win}", message.channel, message.author)

        if Plugin.flip_a_coin():
            redis().incr('hats')
            await send_message("%VAR{cracker_win_hat}", message.channel, message.author)

            if random.randint(0, 4) > 1:
                await send_message("%VAR{cracker_hats}", message.channel, message.author, interpolation_args=[hats])

        if random.randint(0, 10) > 8:
            await send_message("%VAR{cracker_joke}", message.channel, message.author)

        return STOP_PROCESSING

    else:
        # user won
        await send_message("%VAR{cracker_lose}", message.channel, message.author)

        if Plugin.flip_a_coin():
            await send_message("%VAR{cracker_lose_hat}", message.channel, message.author)

            hats = redis().incr(':'.join(['hats', message.author.id]))

            if random.randint(0, 4) == 0:
                if hats > 1:
                    await send_message("%VAR{cracker_your_hats}", message.channel, message.author)

            if random.randint(0, 10) > 8:
                await send_message("%VAR{cracker_joke_tell}", message.channel, message.author)

    return STOP_PROCESSING


Plugin.add_complex(
    name='cracker',
    regexp=r'^_pulls (a|an|another) ((christ|x)mas )?cracker with %bot%',
    chance=100,
    callback=cracker,
)

register_abstract(
    name='cracker_joke',
    lst=[
        "ooh there's a joke:%|%JOKE",
        "oh and a joke%|%JOKE",
        "here's an important christmas joke%|%JOKE",
    ]
)
register_abstract(
    name='cracker_joke_tell',
    lst=[
        "what joke did you get?",
        "what's the joke %={thing :}say?",
    ]
)
register_abstract(
    name='cracker_boom',
    lst=[
        "%VAR{booms}",
        "%VAR{sfx} %VAR{surprises}%!%|what a %={unexpected:weird:strange:odd} sounding cracker",
        "%VAR{sfx} %SMILEY{smile}%!%|what a %={funny:normal} sounding cracker",
        "%VAR{sfx}",
        "%VAR{booms} %SMILEY{bigsmile}",
    ]
)
register_abstract(
	name='cracker_win',
	lst=[
		"%VAR{yays}! i won!%|/got a %VAR{cracker_want_item} %SMILEY{smile}",
		"%VAR{yays}! i won!%|/got a %VAR{cracker_nowant_item}. hmm %SMILEY{sad}",
		"%VAR{yays}! i won!%|i got a %VAR{cracker_want_item} %SMILEY{smile}",
		"%VAR{yays}! i won!%|oh, i got a %VAR{cracker_nowant_item} %SMILEY{sad}",

		"%VAR{yays}! i won a %VAR{cracker_want_item} %SMILEY{smile}%! much better than %={that time:when} i won %VAR{_bmotion_dislike}",
		"%VAR{yays}! i won a... %VAR{cracker_nowant_item} %SMILEY{sad}%! i'd rather have %={had:won} %VAR{_bmotion_like}",

		"%VAR{yays}! i won a %VAR{cracker_want_item} %SMILEY{smile}%! much better than %={that time:when} i won %VAR{_bmotion_dislike}",
		"%VAR{boos}! i won a %VAR{cracker_nowant_item} %SMILEY{sad}%! i'd rather have %={had:won} %VAR{_bmotion_like}",
		"%VAR{boos}! i won a %VAR{cracker_nowant_item} %SMILEY{sad}%! *%={stuffs:shoves:jams:inserts} it up %%*",
		"%VAR{yays}! i won a %VAR{cracker_nowant_item}... but I'd rather have had a %VAR{_bmotion_like}",
		"%VAR{yays}! i won a %VAR{cracker_want_item}%|glad I didn't get a %VAR{_bmotion_dislike}, i hate those",
		"%VAR{yays}! i won a hat!%|/puts on %hisher %VAR{cracker_want_item}",
	]
)
register_abstract(
	name='cracker_lose',
	lst=[
		"%VAR{boos} i didn't win%|%VAR{yays}! you got a %VAR{cracker_neutral_item}",
		"%VAR{boos} i didn't win%|%VAR{yays}! you got a %VAR{cracker_want_item} *jealous*",
		"%VAR{boos} i didn't win%|%VAR{yays}! you got a %VAR{cracker_want_item} %SMILEY{sad} wish i'd got that",
		"%VAR{boos} i didn't win%|oh you got a %VAR{cracker_nowant_item} %SMILEY{smile} glad i didn't get that!",
		"%VAR{boos} you won%|a %VAR{cracker_want_item}! %={ace!:*jealous*:nice!}",
	]
)
register_abstract(
	name='cracker_win_hat',
	lst=[
		"/puts on hat",
		"*hat*",
		"*puts on hat*",
		"woo hat",
		"hats++",
		"/engages hat mode",
		"ACTIVATE THE HAT",
		"ACTIVATE THE GAY%|I mean HAT.%|The keys are like right next to each other",
	]
)
register_abstract(
	name='cracker_lose_hat',
	lst=[
		"don't forget to put on your hat",
		"here's your hat!",
		"put on your hat!",
		"/jams a hat %={on:in:up} %%",
		"hat time!",
	]
)
register_abstract(
	name='cracker_want_item',
	lst=[
		"%VAR{sillyThings:like,strip}",
		"%={large:giant:supersize:XXL:extra-medium:boring:significant:metal:well-manufactured} %VAR{sillyThings:like,strip}",
		"%VAR{scrap_adult_adjectives_t} %VAR{scrap_adult_construction_t:strip}",
		"%VAR{scrap_silly_qualities} %VAR{scrap_silly_adjectives} %VAR{scrap_silly_construction}",
	]
)
register_abstract(
	name='cracker_neutral_item',
	lst=[
		"%VAR{sillyThings:strip}",
		"%={boring:average:medium} %VAR{sillyThings:strip}",
		"%VAR{scrap_adult_adjectives_t} %VAR{scrap_adult_construction_t:strip}",
		"%VAR{scrap_silly_qualities} %VAR{scrap_silly_adjectives} %VAR{scrap_silly_construction}",
		"%VAR{scrap_adjectives} %VAR{scrap_construction:strip}",
	]
)
register_abstract(
	name='cracker_nowant_item',
	lst=[
		"%VAR{sillyThings:dislike,strip}",
		"a %={terrible:small:tiny:insignificant:pointless:plastic:poorly-made:surprise} %VAR{sillyThings:dislike,strip}",
		"a %VAR{scrap_adult_adjectives_t} %VAR{scrap_adult_construction_t:strip}",
		"a %VAR{scrap_silly_qualities} %VAR{scrap_silly_adjectives} %VAR{scrap_silly_construction}",
		"a %VAR{cracker_items}",
	]
)
register_abstract(
	name='cracker_items',
	lst=[
		"shit plastic jumping frog",
		"1:1 magnifying glass",
		"giant paperclip",
		"tape measure which breaks into pieces after 6cm",
		"metal rings puzzle",
		"mini torch with no batteries",
		"terrible joke",
		"mood fish",
	]
)
register_abstract(
	name='cracker_hats',
	lst=[
		"/is now wearing %2 hats",
		"/has %2 hats on",
		"HAT STATUS: %2 hats.",
		"%2 hats %VAR{smile}",
		"Hatometer reading: %2%!%|%heshe cannae take any more hats cap'n!%!%|/B%REPEAT{4:10:O}M",
		"Ahead hat factor %2",
	]
)
register_abstract(
	name='cracker_your_hats',
	lst=[
		"You're now wearing %2 hats.",
		"Woo, %2 hats for you %SMILEY{smile}",
	]
)
register_abstract(
	name='cracker_hats_current',
	lst=[
		"/is wearing %2 hats",
		"/has %2 hats on",
		"%2 hats",
		"/holds up %2 fingers",
	]
)
register_abstract(
	name='cracker_your_hats_current',
	lst=[
		"You've got %2 hats on!",
		"%2, silly",
		"%2, silly%|can't you tell?",
		"%2 hats and %VAR{sillyThings}",
		"%2 hats%!,but one of them looks like %VAR{sillyThings}",
	]
)
register_abstract(
	name='cracker_handle_hats_current',
	lst=[
		"%% has %2 hats on",
		"%2 hats%!, but one of them looks like %VAR{sillyThings}",
	]
)
register_abstract(
	name='cracker_no_hats_own',
	lst=[
		"No hats for me %SMILEY{sad}",
		"0 %SMILEY{sad}",
		"none%!, which is good because i hate hats",
	]
)
register_abstract(
	name='cracker_no_hats_you',
	lst=[
		"no hats for you %SMILEY{sad}%!%|/hugs %%",
		"none",
		"haha, you have no hats%! and everyone else has like %NUMBER{100}000 each",
	]
)
register_abstract(
	name='cracker_no_hats_handle',
	lst=[
		"no hats for %% %SMILEY{sad}",
		"none",
		"what a loser, %% has no hats%! at all",
	]
)


def init(bot: Bot):
    PLUGIN_INIT['redis'] = RedisClient(prefix=REDIS_PREFIX)


"""
bMotion_plugin_add_complex "cracker_hats" "^!hats" 100 bMotion_plugin_complex_cracker_hats "en"

proc bMotion_plugin_complex_cracker_hats { nick host handle channel text } {
	global botnick
	if [regexp -nocase "^!hats (\[^ \]+)" $text matches extranick] {
		set extranick [string trim $extranick]
		bMotion_putloglev d * "Handling !hats for extranick $extranick"
		return [bMotion_plugin_complex_question $nick $host $handle $channel "$botnick: how many hats is $extranick wearing?"]
	}
	bMotion_putloglev d * "Handling !hats for $nick"
	return [bMotion_plugin_complex_question $nick $host $handle $channel "$botnick: how many hats am i wearing?"]
}
"""

"""
bMotion_abstract_add_filter "cracker_hats" "SETTING"
bMotion_abstract_add_filter "cracker_hats" "%s"
bMotion_abstract_add_filter "cracker_hats" "%%"
"""
