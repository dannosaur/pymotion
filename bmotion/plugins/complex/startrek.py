import asyncio
import random
import re

from discord.ext import commands
from discord.ext.commands import Bot

from bmotion.modules.abstracts import register_abstract, get_abstract
from bmotion.plugins import Plugin
from bmotion.plugins.complex import STOP_PROCESSING
from helpers.conf import Config
from helpers.messaging import send_message
from helpers.redis import RedisClient

REDIS_PREFIX = 'plugin:complex:startrek'

PLUGIN_INIT = {}


def bot() -> Bot:
    return PLUGIN_INIT['bot']


def redis() -> RedisClient:
    return PLUGIN_INIT['redis']


async def startrek_cloak(message, matches):
    if redis().get('cloaked'):
        await send_message("Already running cloaked, sir", message.channel, message.author)
    else:
        redis().set('cloaked', True)
        await send_message("/shimmers and disappears from view...", message.channel, message.author)

    return STOP_PROCESSING


async def startrek_decloak(message, matches):
    if not redis().get('cloaked'):
        await send_message("Already decloaked, sir", message.channel, message.author)
    else:
        redis().set('cloaked', False)
        await send_message("/shifts back into view", message.channel, message.author)

    return STOP_PROCESSING


async def startrek_fire(message, matches):
    weapon = matches.group('weapon').lower()
    target = matches.group('target').lower()

    params = {
        'target': target,
        'weapon': weapon,
    }

    if not (weapon_matches := re.match(r'(phasers|torpedoe?|photon|quantum|cheesecake|everything)', weapon)):
        if weapon_matches.group(0).endswith('s'):
            await send_message("I haven't got any '%s' ... I think they %%VAR{felloffs}" % weapon, message.channel, message.author)
        else:
            await send_message("I haven't got any '%s' ... I think it %%VAR{felloffs}", message.channel, message.author)
        return STOP_PROCESSING

    if Plugin.test_regexp(r'%bot%', target):
        await send_message("Don't be so silly, sir.", message.channel, message.author)
        return STOP_PROCESSING

    if redis().get('cloaked'):
        await send_message(f"/swoops in on {target}, decloaking on the way...", message.channel, message.author, params=params)
    else:
        await send_message(f"/swoops in on {target}", message.channel, message.author, params=params)

    if weapon == 'phasers':
        await send_message(get_abstract("phaserfires"), message.channel, message.author, params=params)

    if re.match(r'(torpedoe?s|photon|quantum)', weapon):
        await send_message(get_abstract("torpedofires"), message.channel, message.author, params=params)

    if weapon == 'cheesecake':
        await send_message(get_abstract("cheesecakefires"), message.channel, message.author, params=params)

    if weapon == 'everything':
        await send_message(get_abstract('everythingfires'), message.channel, message.author, params=params)

    if redis().get('cloaked'):
        await send_message("/recloaks", message.channel, message.author, params=params)

    return STOP_PROCESSING


async def startrek_courtmartial(message, matches):
    target = matches.group('target')
    banzai = matches.group('with_banzai') is not None
    print("target:", target)
    print("banzai?", banzai)

    if Plugin.test_regexp(r'%bot%', target):
        await send_message("Duh.", message.channel, message.author)
        return STOP_PROCESSING

    if not re.match(r'<.*>', target):
        await send_message("Please specify the full name of someone in the channel (@tag them)", message.channel, message.author)
        return STOP_PROCESSING

    redis().set('banzai_mode_brig', banzai)

    if redis().get('brig'):
        await send_message("I'm sorry Sir, I already have someone in the brig - please try again later, or empty the Recycle Bin", message.channel, message.author)
        return STOP_PROCESSING

    if banzai:
        bot().add_command(brig_vote_handler)

        await send_message("%VAR{brigbanzais}", message.channel, message.author, params={'target': target}, use_timers=False)
        await send_message("Rules simple. Simply decide if you think I'll find {target} innocent", message.channel, message.author, params={'target': target}, use_timers=False)

        redis().set('brig_innocent', [])
        redis().set('brig_guilty', [])

        await send_message("Please bets now! (`{cmd_prefix}bet innocent` and `{cmd_prefix}bet guilty`, one bet per person)", message.channel, message.author, use_timers=False)

    await send_message("/throws {target} in the brig to await charges", message.channel, message.author, params={'target': target}, use_timers=False)
    redis().set('brig', target)

    if redis().get('banzai_mode_brig'):
        asyncio.ensure_future(banzai_brig_mid_handler(message.channel))

    asyncio.ensure_future(do_brig(message.channel))

    return STOP_PROCESSING


async def do_brig(channel):
    await asyncio.sleep(Config.getpath('startrek.brig_delay'))

    brig = redis().get('brig')
    banzai = redis().get('banzai_mode_brig')
    if not brig:
        return

    if banzai:
        await send_message("Betting ends!", channel, use_timers=False)
        bot().remove_command('bet')

        innocent_votes = redis().get('brig_innocent')
        guilty_votes = redis().get('brig_guilty')
        if len(guilty_votes) == len(innocent_votes):
            guilty = random.choice([True, False])
        else:
            guilty = len(guilty_votes) > len(innocent_votes)
    else:
        guilty = random.choice([True, False])

    await send_message("%s, you are charged with %%VAR{charges} and %%VAR{charges}" % brig, channel, use_timers=False)

    if guilty:
        await send_message("You have been found guilty, and are sentenced to %VAR{punishments}. And may God have mercy on your soul.", channel, use_timers=False)

        if banzai:
            if len(redis().get('brig_guilty')) > 0:
                await send_message("Congratulations go to the big winners. Well done! Riches beyond your wildest dreams are yours for the taking!", channel, use_timers=False)
    else:
        await send_message("You have been found innocent. Have a nice day.", channel, use_timers=False)

        if banzai:
            if len(redis().get('brig_innocent')) > 0:
                await send_message("Congratulations go to the big winners. Well done! Glory and fame are yours!", channel, use_timers=False)

    # cleanup
    redis().set('brig', False)
    redis().set('banzai_mode_brig', False)
    redis().set('brig_innocent', [])
    redis().set('brig_guilty', [])


async def banzai_brig_mid_handler(channel):
    await asyncio.sleep(Config.getpath('startrek.brig_delay') / 2 + 7)
    await send_message(get_abstract('banzaimidbets'), channel, use_timers=False)


@commands.command(name='bet')
async def brig_vote_handler(ctx, arg):
    if not redis().get('brig'):
        print("nobody in the brig?")
        bot().remove_command('bet')
        return

    voters = redis().get('brig_innocent') + redis().get('brig_guilty')
    if ctx.author.id in voters:
        await send_message(f"{ctx.author.mention}, you have already bet!", ctx.message.channel, use_timers=False)
        await ctx.message.add_reaction("❌")
        return

    if arg.lower() == 'innocent':
        print(f"accepted 'innocent' vote from {ctx.author}")
        innocent = redis().get('brig_innocent')
        innocent.append(ctx.author.id)
        redis().set('brig_innocent', innocent)
        await ctx.message.add_reaction("👍")
        return

    if arg.lower() == 'guilty':
        print(f"accepted 'guilty' vote from {ctx.author}")
        guilty = redis().get('brig_guilty')
        guilty.append(ctx.author.id)
        redis().set('brig_guilty', guilty)
        await ctx.message.add_reaction("👍")
        return

    await send_message(f"'{arg}' is not a valid option. Choose from 'innocent' or 'guilty'", ctx.message.channel)


@commands.command('flushbrig')
async def brig_flush(ctx):
    brig = redis().get('brig')
    if not brig:
        await send_message("There's nobody in the brig, sir.", ctx.message.channel, ctx.message.author)
        return

    await send_message(("Whoops... I forgot {target} was in the brig", "/sweeps corpse under the rug"), ctx.message.channel, ctx.message.author, params={'target': brig})
    redis().set('brig', None)


Plugin.add_complex(name='st-cloak', regexp=r'^%bot% ?:?,? +cloak$', chance=100, callback=startrek_cloak)
Plugin.add_complex(name='st-decloak', regexp=r'^%bot% ?:?,? +decloak$', chance=100, callback=startrek_decloak)
Plugin.add_complex(name='st-fire', regexp=r'^%bot% ?:?,? +fire (?P<weapon>.*) at (?P<target>.*)$', chance=100,
                   callback=startrek_fire)
Plugin.add_complex(name='st-courtmartial',
                   regexp=r'^%bot% ?:?,? +courtmartial (?P<target>[^\s]*)(?P<with_banzai> with banzai)?', chance=100,
                   callback=startrek_courtmartial)

register_abstract(
    name='phaserfires',
    lst=[
        "/fires several shots from the forward phaser banks, disabling {target}",
        "/fires several shots from the forward phaser banks, destroying {target}",
        "/flies out through the explosion in an impressive bit of piloting (not to mention rendering %SMILEY{smile}",
        "/accidentally activates the wrong system and replicates a small tree",
        ("/misses a gear and stalls", "%VAR{oops}"),  # |%bot\[50,�VAR{ruins}\]",
        "/uses attack pattern alpha, spiralling towards {target}, firing all phaser banks",
        "_{target} is blown to pieces as {me} flies off into the middle distance_",
        "/anchors {target} to a small asteriod, paints a target on their upper hull, and fires a full phaser blast at them",
        "/rolls over, flying over {target} upside down, firing the dorsal phaser arrays on the way past",
        "/flies around {target}, firing the ventral arrays",
        "/jumps to high impulse past {target} and fires the aft phaser banks",
        "System failure: TA/T/TS could not interface with phaser command processor (ODN failure)",
        "/pulls the Picard move (the non-uniform one)",
    ]
)

register_abstract(
    name='torpedofires',
    lst=[
        "/fires a volley of torpedos at {target}",
        "/breaks into a roll and fires torpedos from dorsal and ventral launchers in sequence",
        ("/breaks into a roll and ties %himherself in a knot", "Damn."),  # "%bot\[50,�VAR{ruins}\]",
        "System failure: TSC error",
        "/flies past %% and fires a full spread of torpedos from the aft launchers",
        ("/heads directly for {target}, firing a full spread of torpedos from the forward lauchers",
         "/flies out through the wreckage"),
    ]
)

register_abstract(
    name='cheesecakefires',
    lst=[
        ("SAVE THE CHEESECAKE", "/eats cheesecake"),
        "/splurges the cheesecake at {target}",
        "/rams cheesecake down {target}'s throat",
        "/throws a large %VAR{fruits} cheesecake directly at {target}",
        "/lobs a %VAR{colours} %VAR{fruits} cheesecake at {target}",
        "/launches a fleet of %VAR{colours} %VAR{fruits} cheesecakes at {target}",
    ]
)

register_abstract(
    name='everythingfires',
    lst=[
        "/opens the cargo hold and ejects some plastic drums at {target}",
        "/launches all the escape pods",
        "/fires the Universe Gun(tm) at {target}",
        "/launches some torpedos and fires all phasers",
        "/shoots a little stick with a flag reading 'BANG' on it out from the forward torpedo launchers",
        "/lobs General Darian at {target}",
    ]
)

register_abstract(
    name='brigbanzais',
    lst=[
        "The {target} Being In Brig Bet!",
        "The Naughty {target} Charge Conundrum!",
        "{target}'s Prison Poser!",
    ]
)

register_abstract(
    name='banzaimidbets',
    lst=[
        "bet bet bet!",
        "bet now! Time running out!",
        "come on, bet!",
        "what you waiting for? bet now!",
        "you want friends to laugh at you? Bet!",
    ]
)

register_abstract(
    name='charges',
    lst=[
        "exploding %VAR{treknouns}",
        "setting fire to %VAR{treknouns}",
        "gross incompetence",
        "teaching the replicators to make decaffinated beverages",
        "existing",
        "misuse of %VAR{treknouns}",
        "improper use of %VAR{treknouns}",
        "improper conduct with %VAR{treknouns}",
        "plotting with %VAR{treknouns}",
        "doing warp 5 in a 3 zone",
        "phase-shifting %VAR{treknouns}",
        "having sex on %VAR{treknouns}",
        "having sex with %VAR{treknouns}",
        "attempting to replicate %VAR{treknouns}",
        "terraforming %VAR{treknouns}",
        "putting %VAR{treknouns} into suspended animation",
        "writing a character development episode",
        "time travelling without a safety net",
    ]
)


def init(bot: Bot):
    PLUGIN_INIT['bot'] = bot
    PLUGIN_INIT['redis'] = RedisClient(prefix='plugin:complex:startrek')

    bot.add_command(brig_flush)
