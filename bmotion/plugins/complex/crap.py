from discord.ext import commands
from discord.ext.commands import Bot

from bmotion.modules.abstracts import register_abstract
from helpers.messaging import send_message


@commands.command(name='crap')
async def crap(ctx, *args):
    await send_message("%VAR{random_crap_main}", ctx.message.channel)


def init(bot: Bot):
    bot.add_command(crap)


register_abstract(
    name='random_crap_main',
    lst=[
        "/does a %VAR{random_crap_adj} %VAR{random_crap_type} and hands it to %ruser{enemy}",
        ("/does a %VAR{random_crap_adj} %VAR{random_crap_type} and hands it to %ruser{enemy}", "present!"),
        "/crimps off a %VAR{random_crap_adj} %VAR{random_crap_type} and hands it to %ruser{enemy}",
        ("/crimps off a %VAR{random_crap_adj} %VAR{random_crap_type} and hands it to %ruser{enemy}", "present"),
        ("/gift wraps a %VAR{random_crap_adj} %VAR{random_crap_type}", "/ships it first class to %ruser{enemy}"),
        "/injects a %VAR{random_crap_adj} %VAR{random_crap_type} into a padded envelope and posts it to %ruser{enemy}",
        ("/does a %VAR{random_crap_adj} %VAR{random_crap_type} in a flan base", "/cooks it", "Here you go, %ruser{enemy}, I made you this flan!"),
    ]
)
register_abstract(
    name='random_crap_adj',
    lst=[
        "lemon-flavoured",
        "tasty",
        "%VAR{colours}",
        "delicious",
        "yummy",
        "uncomfortable",
        "comforting",
    ]
)
register_abstract(
    name='random_crap_type',
    lst=[
        "poop",
        "crap",
        "shit",
        "dingleberry",
        "dollop",
        "poo",
    ]
)
register_abstract(
    name='randomStuff',
    lst=[
        "%VAR{random_crap_main}",
    ]
)
