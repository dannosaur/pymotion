from bmotion.plugins import Plugin

Plugin.add_simple(
    name='zzz',
    regexp=r'^zzz+',
    chance=50,
    responses=[
        "/hands {user_tag} a coffee",
        "wake up%colen",
        "go to bed already",
        "sorry, are we keeping you up?",
        "you need a coffee",
        "/throws water over {user_tag} to wake them up",
        "/lends {user_tag} a pillow",
        "/lends {user_tag} a cushion",
        "/hands {user_tag} some ProPlus",
    ],
)
