import random

from discord import Message
from discord.ext.commands import Cog

from bmotion.modules import system
from bmotion.plugins import Plugin, BaseCog
from helpers.messaging import send_message


class SimplePluginCog(BaseCog):
    plugin_type = 'simple'

    @Cog.listener()
    async def on_message(self, message: Message):
        if message.author.id == self.bot.user.id:
            return

        if message.author.id in system.IGNORES:
            return

        if plugin := Plugin.find_simple(message.content):
            print(f"Executing simple plugin: {plugin['name']}")
            await send_message(random.choice(plugin['responses']), message.channel, message.author)
