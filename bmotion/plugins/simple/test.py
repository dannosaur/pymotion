from bmotion.modules.abstracts import register_abstract
from bmotion.plugins import Plugin

Plugin.add_simple(
    name='test',
    regexp=r'^test$',
    chance=100,
    responses=[
        '%VAR{test}',
    ],
)

register_abstract(
    name='test',
    lst=[
        # '%VAR{ididnt}',
        # '%REPEAT{4:10:bl}',
        # "D%REPEAT{5:10:u}de!"
        # '%SMILEY{sad}',
        # 'M%REPEAT{2:8:o}%REPEAT{2:8:O}%REPEAT{2:8:0}%REPEAT{2:8:o}%REPEAT{2:8:0}%REPEAT{2:8:o}!'
        # "%VAR{sillyverbs:presentpart} %VAR{sillythings}: you're doing it wrong",
        # '%NUMBER{100}',
        # "%ruser{enemy}",
        # "loookkk ooouuuttt beellooww!%|*CRUMP*%|ow %SMILEY{sad}"
        # "/flattens %%",
        "/does a triple inverted twist somersault and lands neatly next to %%"
    ]
)