from bmotion.modules.abstracts import ABSTRACTS
from bmotion.plugins import Plugin

Plugin.add_simple(
    name='here',
    regexp=r'^any ?(one|body) (here|alive|talking)',
    chance=100,
    responses=ABSTRACTS["no's"],
)
