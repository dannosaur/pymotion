from bmotion.plugins import Plugin

Plugin.add_simple(
    name='fish',
    regexp=r'^fish!?',
    chance=5,
    responses=[
        "today's fish is trout a la creme. enjoy your meal",
    ],
)
