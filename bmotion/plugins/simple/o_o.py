from bmotion.plugins import Plugin

Plugin.add_simple(
    name='o_o',
    regexp=r'^o[\._]o$',
    chance=10,
    responses=[
        "O.O",
        "O.o",
        "o.O",
        "o.o",
        "O_O",
        "O_o",
        "o_O",
        "o_o",
    ],
)
