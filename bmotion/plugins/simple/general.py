"""

#                         name  regexp            %  responses
#bMotion_plugin_add_simple "url-gen" {(http|ftp)://([[:alnum:]]+\.)+[[:alnum:]]{2,3}} 15 "%VAR{bookmarks}" "en"
bMotion_plugin_add_simple "ali g" "^((aiii+)|wikkid|innit|respect|you got me mobile|you iz)" 40 "%VAR{aiis}" "en"
bMotion_plugin_add_simple "wassup" "^wa+((ss+)|(zz+))u+p+(!*)?$" 40 "wa%REPEAT{4:12:a}up!" "en"
bMotion_plugin_add_simple ":(" "^((:|;|=)(\\\(|\\\[))$" 40 "%VAR{boreds}" "en"
#kis moved the following line to complex_questions because it was breaking stuff.
#kis bMotion_plugin_add_simple "question-have" "^%botnicks:?,? do(n'?t)? you (like|want|find .+ attractive|get horny|(find|think) .+ (is ?)horny|have|keep)" 100 "%VAR{yesnos}" "en"
bMotion_plugin_add_simple "asl-catch" {[0-9]+%slash[mf]%slash.+} 75 "%VAR{greetings}" "en"
bMotion_plugin_add_simple "sing-catch" {^#.+#$} 40 [list "no singing%colen" "shh%colen"] "en"
bMotion_plugin_add_simple "no-mirc" "mirc" 5 [list "mIRC < irssi" "use irssi" "mmm irssi" "irssi > *" "/fires %% into the sun"] "en"
bMotion_plugin_add_simple "no-bitchx" "bitchx" 5 [list "bitchx < irssi" "use irssi" "mmm irssi" "irssi > *" "/fires %% into the sun"] "en"
bMotion_plugin_add_simple "no-trillian" "trillian" 5 [list "trillian < irssi" "use trillian + bitlbee" "mmm irssi" "irssi > *" "/fires %% into the sun"] "en"
bMotion_plugin_add_simple "right" "%botnicks: (i see|ri+ght|ok|all? ?right|whatever)" 60 [list "it's true %VAR{unsmiles}" "it's true%colen" "yes" "what" "you don't believe me?"] "en"

bMotion_plugin_add_simple "likea-catch" "i do love a good (.+)" 30 [list "me too %VAR{smiles}" "same!" ] "en"
bMotion_plugin_add_simple "secretcatch" "(secret|sekrit)" 40 [list "we all know %ruser likes to %VAR{dVerbs} themselves with a %VAR{dNouns} %VAR{smiles}" "it was %ruser!" "%VAR{ididntresponses}" ] "en"
bMotion_plugin_add_simple "andthan-catch" "and( then| than)" 10 [list "and then a %VAR{animals} came" "and then a bear came" "and then my %VAR{bodypart} %VAR{fellOffs}" "and thaaan?"] "en"
bMotion_plugin_add_simple "the-r" "^r$" 30 [list "%VAR{theRs}" ] "en"
bMotion_plugin_add_simple "answerR" "\\mr %botnicks" 50 [list "%VAR{greetings}" "%VAR{theRs}" ] "en"
bMotion_plugin_add_simple "lookrobot" "(look robot|look,? robot|%botnicks look|look,? %botnicks)" 100 [list "I've seen it before, it's rubbish" ":o" "omg" "^_^" ] "en"

bMotion_plugin_add_simple "energydrink" "^!energy(drink)?" 100 [list "%VAR{energydrink}"] "en"
bMotion_plugin_add_simple "frysdog1" "^what do we want\[?!\]*$" 90 [list "Fry's dog!"] "en"
bMotion_plugin_add_simple "frysdog2" "^when do we want it\[?!\]*$" 90 [list "Fry's dog!"] "en"
"""
from bmotion.modules.abstracts import ABSTRACTS
from bmotion.plugins import Plugin

Plugin.add_simple(name='cunt', regexp=r'^(cunt|twat|bollocks|fuck|shit)$', chance=80, responses=["cunt", "twat", "bollocks", "bastards", "idiot", "wanker", "%VAR{prom_first}", "pile of %VAR{prom_first}", "%VAR{prom_first} %VAR{prom_first}ers"])
# Plugin.add_simple(name='url-img', regexp=r'(?:http\:|https\:)?\/\/.*\.(?:png|jpg|jpeg|gif)', chance=25, responses=ABSTRACTS.get('rarrs'))
Plugin.add_simple(name='ow', regexp=r"^(ow+|ouch|aie+)!*$", chance=50, responses=ABSTRACTS.get('awwws'))
Plugin.add_simple(name='oops', regexp=r"^(oops|who+ps|whups|doh |d\'oh)", chance=40, responses=ABSTRACTS.get('ruins'))
Plugin.add_simple(name='luck', regexp=r"wish me (good )?luck", chance=90, responses=["Luck!", "good luck!", ("bad luck!", "no wait, good luck!"), "i hope it doesn't explode", "%NUMBER{100}% good luck", "Luck you say? ... hmm, it's possible. I can have that with you in 6-8 weeks", "/looks for falling star in order to make luck based wish"])
Plugin.add_simple(name='lolcats', regexp=r"^i can has ", chance=60, responses=[("no you fucking can't", "not until you learn to use english right, anyway"), "what", "i can has grammar plz", ("i has a bucket", "I have placed it over your stupid head."), "no you fucking can't", "shut up", ("everyone look at {user_tag}, they're up to date on all internet memes!", "for example, {user_tag}: all your base are belong to us", "fantastic.")])
Plugin.add_simple(name='bof', regexp=r'^bof$', chance=30, responses=ABSTRACTS.get('french'))
Plugin.add_simple(name='alors', regexp=r'^alors$', chance=30, responses=ABSTRACTS.get('french'))
Plugin.add_simple(name='bonjour', regexp=r'bonjour', chance=20, responses=ABSTRACTS.get('french'))
Plugin.add_simple(name='moo', regexp=r'^mooo*!*$', chance=40, responses=ABSTRACTS.get('moos'))
Plugin.add_simple(name='foo', regexp=r'^foo$', chance=30, responses=['bar'])
Plugin.add_simple(name='bar', regexp=r'^bar$', chance=30, responses=['baz'])
Plugin.add_simple(name='hal', regexp=r'%bot%: open the cargo bay doors?', chance=70, responses=["I'm sorry {user_tag}, I can't do that"])
Plugin.add_simple(name='only4', regexp=r'^only [0-9]+', chance=80, responses=["well actually %NUMBER{100}", "that's quite a lot", "that's not very many", "well that's usually enough to get me functioning"])
Plugin.add_simple(name='team', regexp=r'there\'s no [\'\"]?i[\'\"]? in [\'\"]?team[\'\"]?', chance=80, responses=["But there's a U in cunt"])
Plugin.add_simple(name='centipedes', regexp=r'[a-z]+\? in my [a-z]+\?', chance=70, responses=["it's more likely than you think"])
Plugin.add_simple(name='cilit', regexp=r'they\'re a challenge', chance=80, responses=["but not... for Cilit Bang!"])
Plugin.add_simple(name='choose', regexp=r'choose (.+)', chance=30, responses=["Choose Life.", "Choose a job.", "Choose a career.", "Choose a family.", "Choose a fucking big television", "choose washing machines", "choose cars", "choose compact disc players", "choose electrical tin openers", "Choose good health", "choose low cholesterol", "choose dental insurance", "choose fixed interest mortgage repayments", "Choose a starter home.", "Choose your friends.", "Choose leisurewear and matching luggage.", "Choose a three-piece suite on hire purchase in a range of fucking fabrics.", "Choose DIY and wondering who the fuck you are on a Sunday morning.", "Choose sitting on that couch watching mind-numbing, spirit-crushing game shows, stuffing fucking junk food into your mouth.", "Choose rotting away at the end of it all, pishing your last in a miserable home, nothing more than an embarrassment to the selfish, fucked up brats you spawned to replace yourself."])
Plugin.add_simple(name='cartman1', regexp=r'not (every|all) of them', chance=40, responses=[("no, but most of them are", "and all it takes is most of them...")])
Plugin.add_simple(name='luggage', regexp=r'^[0-9]{5}$', chance=40, responses=["amazing, that's the same combination as my luggage!"])
Plugin.add_simple(name='nicecar1', regexp=r'^nice [^ ]+[a-rt-z]!?$', chance=40, responses=['wanna show me what it can do?'])
Plugin.add_simple(name='nicecar2', regexp=r'^nice [^ ]+s!?$', chance=40, responses=["wanna show me what they can do?"])
Plugin.add_simple(name='didnt', regexp=r'^i didn\'t\!?$', chance=40, responses=ABSTRACTS.get('ididnt'))
Plugin.add_simple(name='transform', regexp=r'^%bot%:?,? transform and roll out', chance=100, responses=["/transforms into %VAR{sillythings} and rolls out"])
Plugin.add_simple(name='bored', regexp=r'i\'m bored', chance=40, responses=ABSTRACTS.get('bored'))
Plugin.add_simple(name='dude', regexp=r'^Dude!$', chance=40, responses=ABSTRACTS.get('sweet'))
Plugin.add_simple(name='sweet', regexp=r'^Sweet!$', chance=40, responses=ABSTRACTS.get('dude'))
Plugin.add_simple(name='letme', regexp=r'(%bot%(.,)? let me know when)|(let me know when(.,)? %bot%)', chance=100, responses=["r", "will do", "sure", "np", "ok"])
Plugin.add_simple(name='iknowkungfu', regexp=r'i know kung ?fu', chance=100, responses=[("Show me", "hmh.. your style is un-impressive"), ("Show me", "hmh.. your style is impressive")])

Plugin.add_simple(name='mmm', regexp=r'mmm+ %bot$', chance=25, responses=["%SMILEY{smile}"])
Plugin.add_simple(name='seven', regexp=r'^7[?!.]?$', chance=40, responses=["7!", "7 %SMILEY{smile}", "wh%REPEAT{3:7:e} 7!"])
Plugin.add_simple(name='coffee', regexp=[r'coffee ?< ?', r'[a-z0-9]+ ?> ?coffee', r'coffee (is )?(teh |the )?(suck|rubbish|fail|horrible|horrid)'], chance=90, responses=["what", "%SMILEY{sad}", "%VAR{kills}", "%VAR{smacks} {user_tag}", ("you li%REPEAT{1:5:e}", "YOU LI%REPEAT{4:10:E}%REPEAT{3:5:!}%colen")])




