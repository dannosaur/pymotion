from bmotion.plugins import Plugin

Plugin.add_simple(name='rocket1', regexp=r'^prepare for trouble', chance=100, responses=["... and make it double"])
Plugin.add_simple(name='rocket2', regexp=r'^and make it double', chance=100, responses=["To protect the world from devastation"])
Plugin.add_simple(name='rocket3', regexp=r'^to protect the world from devastation', chance=100, responses=["To unite all people within our nation"])
Plugin.add_simple(name='rocket4', regexp=r'^to unite all people (with)?in our nation', chance=100, responses=["to denounce the evil of truth and love"])
Plugin.add_simple(name='rocket5', regexp=r'^to denounce the evils? of truth and love', chance=100, responses=["to extend our reach to the stars above"])
Plugin.add_simple(name='rocket6', regexp=r'^to extend our reach to the stars above', chance=100, responses=["Jessie!"])
Plugin.add_simple(name='rocket7', regexp=r'^jessie!', chance=100, responses=["James!"])
Plugin.add_simple(name='rocket8', regexp=r'^team rocket blast off at the speed of light', chance=100, responses=[("Surrender now or prepare to fight!")])  # "%BOT\[Get a life.\]"
