from bmotion.plugins import Plugin

Plugin.add_simple(
    name='numberwang',
    regexp=r'^[0-9]+!*$',
    chance=5,
    responses=[
        "{user_tag}: that's numberwang!",
        "{user_tag}: that's wangernumb!",
    ],
)
