from bmotion.plugins import Plugin

Plugin.add_simple(
    name='commaeight',
    regexp=r'\d+,8,1',
    chance=60,
    responses=[
        ("SEARCHING FOR *", "LOADING", "READY."),
    ],
)
