from bmotion.modules.abstracts import ABSTRACTS
from bmotion.plugins import Plugin

Plugin.add_simple(
    name='littlebit',
    regexp=r'(what, )?not even a little bit',
    chance=5,
    responses=ABSTRACTS["go on then"],
)
