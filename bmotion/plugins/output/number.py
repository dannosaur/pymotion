import logging
import random
import re

from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.number')


class NumberPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%NUMBER{(?P<number>\d+)}',
            lambda m: str(random.randint(0, int(m.group('number')))),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response


Plugin.add_output('number', NumberPlugin)
