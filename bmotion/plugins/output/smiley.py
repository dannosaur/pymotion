import logging
import random
import re

from bmotion.modules.mood import get_mood
from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.smiley')


class SmileyPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%SMILEY{(?P<smiley_type>[^}]+)}',
            lambda m: self.get_smiley(m.group('smiley_type')),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response

    def get_smiley(self, smiley_type):
        nose_type = random.choice(["none", "dash", "o"])
        eyes_type = random.choice(["colon", "equals", "default"])
        mouth_type = random.choice(["paren", "bracket", "angle"])

        nose = ''
        if nose_type == 'dash':
            nose = '-'
        elif nose_type == 'o':
            nose = 'o'

        eyes = ''
        if eyes_type in ['colon', 'default']:
            eyes = ':'
        elif eyes_type == 'equals':
            eyes = '='

        smilies = ''
        if mouth_type == 'paren':
            smilies = list(')D(CDoO/x(39')
        elif mouth_type == 'bracket':
            smilies = list(']D[CDoO/x[39')
        elif mouth_type == 'angle':
            smilies = list('>D<CDoO/x<39')

        if smiley_type == 'auto':
            happy = get_mood('happy')
            if happy < -10:
                smiley_type = 'bigsad'
            elif happy < 0:
                smiley_type = 'sad'
            elif happy < 10:
                smiley_type = 'smile'
            else:
                smiley_type = 'bigsmile'

        reverse = False
        termlist = ["smile", "bigsmile", "sad", "bigsad", "horror", "surprise", "bigsurprise", "uneasy", "embarrassed",
                    "cry", "cat", "yum"]
        index = termlist.index(smiley_type)
        if index < 0:
            return ''

        smile = smilies[index]
        if smiley_type == 'horror':
            reverse = True
        elif smiley_type == 'cry':
            nose = "'"

        if reverse:
            return f'{smile}{nose}{eyes}'
        else:
            return f'{eyes}{nose}{smile}'


Plugin.add_output('smiley', SmileyPlugin)
