import logging
import random
import re

from bmotion.modules.abstracts import get_abstract
from bmotion.plugins import Plugin
from bmotion.plugins.complex.facts import get_fact, FactsNotFound
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.number')


def get_answer(fact_what):
    try:
        return get_fact(fact_what)
    except FactsNotFound:
        return get_abstract('sillyThings')


class JokePlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%JOKE',
            lambda m: self.create_joke(),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response

    def create_joke(self):
        # %r is relational
        # %n is random noun
        jokes = [
            ("what's the difference between %n and %n?", "%={one's %r and the other's %r:not that much:more than you could possibly imagine!}"),
            ("what's the difference between %n and %n?", "one's %n and the other's %n"),
            ("what do you get when you cross %n and %n?", "%r with %r!"),
            ("what do you get when you cross %n and %n?", "%r"),
            ("what do you think you could do with %n?", "become %r"),
            ("how do you make %n?", "use %r"),
            ("did you hear the one about %n?", "it was %r"),
            ("what's %VAR{colours} and invisible?", "%={no:an invisible} %VAR{sillyThings:strip}"),
            ("what's %VAR{colours} and sticky?", "%n"),
            ("how many %VAR{sillyThings:strip,plural} does it take to change a light bulb?", "%NUMBER{10} to hold the %VAR{sillyThings:strip} and %NUMBER{15} to %VAR{dVerbs} it"),
            ("how many %VAR{sillyThings:strip,plural} does it take to change %VAR{sillyThings}?", "%NUMBER{50}"),
            ("what do you do if a blonde throws %VAR{sillyThings} at you?", "pull the %VAR{sillyThings:strip} out and throw it back!"),
            ("what do you do if a blonde throws %VAR{sillyThings} at you?", "%VAR{sillyVerbs}"),
            ("what noise does a %VAR{animals} with no %VAR{bodypart:plural} make?", "%VAR{sound2}"),
            ("yo momma so fat, when she %VAR{sillyVerbs:present}, %VAR{sillyThings:strip,plural} %VAR{sillyVerbs}!",)
        ]

        joke = random.choice(jokes)
        processed_joke = []
        for line in joke:
            new_line = line
            new_line = re.sub(r'%n', lambda m: get_abstract('sillyThings'), new_line)
            new_line = re.sub(r'%r', lambda m: get_answer(), new_line)
            processed_joke.append(new_line)

        if len(processed_joke) > 1:
            processed_joke.insert(1, '%DELAY')

        return '%|'.join(processed_joke)


Plugin.add_output('joke', JokePlugin)
