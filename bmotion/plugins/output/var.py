import logging
import re

import inflect
from nltk.stem.wordnet import WordNetLemmatizer

from bmotion.modules.abstracts import get_abstract, redis
from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin
from helpers import nlp

logger = logging.getLogger('plugins.output.var')


class VarPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%VAR{(?P<abstract>\w+)(:(?P<options>[^}]+))?}',
            lambda m: self._process_options(m.group('abstract'), m.group('options')),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response

    def _process_options(self, abstract, options):
        text = get_abstract(abstract)
        if options:
            options = options.split(':')
            for option in options:
                text = self._process_option(text, option, abstract)

        return text

    def _process_option(self, text, option, abstract):
        match option:
            case 'prev':
                return redis().get(f'output:var:last:{abstract}')
            case 'prevtriplet':
                return redis().get(f'output:var:last:pretriplet:{abstract}')
            case 'strip':
                return re.sub(r'(an?|the|some|his|her|their)', '', text)
            case 'verb':
                return self.do_makeverb(text)
            case 'past':
                return WordNetLemmatizer().lemmatize(text, 'v')
            case 'presentpart':
                return self.do_presentpart(text)
            case 'present':
                return self.do_simple_present(text)
            case 'plural':
                return inflect.engine().plural(text)
            case 'title':
                return text.title()
            case 'camel':
                return self.do_camelcase(text)
            case 'owner':
                return text
            case 'removeowner':
                return text
            case 'underscore':
                return text.replace(' ', '_')
            case 'caps':
                return text.upper()
            case 'triplet':
                return text
            case 'like':
                return text
            case 'dislike':
                return text
            case 'lower':
                return text.lower()

    def do_plural(self, text):
        return text

    def do_simple_present(self, text):
        matches = re.search(r'^(?P<verb>\w+)(?P<extra> (.+))?', text)
        verb = matches.group('verb')
        extra = matches.group('extra') or ''

        return verb + 's' + extra

    def do_past_tense(self, text):
        doc = nlp.nlp(text)
        for i in range(len(doc)):
            token = doc[i]
            if token.tag_ in ['VBP', 'VBZ']:
                # print(token.text, token.lemma_, token.pos_, token.tag_)
                text = text.replace(token.text, token._.make_past)
        # print(text)
        return text

    def do_makeverb(self, text):
        if re.search(r'([sx])$', text):
            return text

        if matches := re.search(r'^(?P<root>.*)y$', text):
            verb = matches.group('root')
            verb += 'ies'
            return verb

        return text + 's'

    def do_camelcase(self, text):
        s = re.sub(r"([_\-])+", " ", text).title().replace(" ", "")
        return ''.join([s[0].lower(), s[1:]])

    def do_presentpart(self, text):
        matches1 = re.match(r"^(\w+) (.+)?", text, re.IGNORECASE)
        if matches1:
            verb, extra = matches1.groups()

            matches2 = re.match(r"(.+[^i])e$", verb, re.IGNORECASE)
            if matches2:
                a = matches2.group(1)
                return f"{a}ing {extra}"

            matches3 = re.match(f"(.+[aeiou])([^aeiouy])$", verb, re.IGNORECASE)
            if matches3:
                a, b = matches3.groups()
                return f"{a}{b}{b}ing {extra}"

            return f"{verb}ing {extra}"


Plugin.add_output('var', VarPlugin)
