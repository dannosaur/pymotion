import logging
import random
import re

from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.repeat')


class RepeatPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%REPEAT{(?P<min>\d+):(?P<max>\d+):(?P<what>[^}]*)}',
            lambda m: m.group('what') * random.randint(int(m.group('min')), int(m.group('max'))),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response


Plugin.add_output('repeat', RepeatPlugin)
