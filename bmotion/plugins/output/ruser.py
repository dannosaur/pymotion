import logging
import re
from pprint import pprint
from typing import Union

from discord import Member
from discord.ext.commands import Bot

from bmotion.modules.abstracts import get_abstract
from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.ruser')


class RuserPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        if matches := re.search(r'%ruser(?P<allopts>{(?P<filter>[a-z]*)?(?P<optstring>:(?P<options>[a-z,]+))?})?', response):
            allopts = matches.group('allopts')
            _filter = matches.group('filter')
            optstring = matches.group('optstring')
            options = matches.group('options')
            logger.debug(f"Found %ruser with filter={_filter} and options={options}")

            ruser = await self.get_random_user(_filter)
            ruser = ruser.display_name

            if options:
                options_list = options.split(',')
                for option in options_list:
                    if option == 'owner':
                        ruser = self.make_possessive(ruser)
                    elif option == 'caps':
                        ruser = ruser.upper()
                    else:
                        print(f"Unexpected option {option} in {matches.groups()}")
            response = re.sub(matches.group(0), ruser, response)

        logger.debug(f"Processed response: {response}")
        return response

    def make_possessive(self, text, alt_mode=False):
        if text == "":
            return "someone's"

        if text == "me":
            if alt_mode:
                return "mine"
            return "my"

        if text == "you":
            if alt_mode:
                return "yours"
            return "your"

        if text.endswith('s'):
            return f"{text}'"
        return f"{text}'s"


Plugin.add_output('ruser', RuserPlugin)
