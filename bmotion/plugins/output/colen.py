import logging
import random
import re

from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.colen')


class ColenPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%colen',
            lambda m: ''.join(random.choice('!@#$%^&*()') for _ in range(random.randint(1, 12) + 5)),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response


Plugin.add_output('colen', ColenPlugin)
