import logging
import random
import re

from bmotion.plugins import Plugin
from bmotion.plugins.output import OutputPlugin

logger = logging.getLogger('plugins.output.repeat')


class SwitchPlugin(OutputPlugin):
    async def _process_response(self, response: str):
        logger.debug(f"Processing response: {response}")
        response = re.sub(
            r'%={(?P<options>[^}]*)}',
            lambda m: random.choice(m.group('options').split(':')),
            response,
        )
        logger.debug(f"Processed response: {response}")
        return response


Plugin.add_output('switch', SwitchPlugin)
