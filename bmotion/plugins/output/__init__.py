import random
from pprint import pprint
from typing import Union

from discord import TextChannel, Member
from discord.ext.commands import Bot

from bmotion.plugins import BaseCog
from bmotion.modules import friendship
from bmotion.modules import userinfo
from helpers.redis import RedisClient


class OutputPluginCog(BaseCog):
    plugin_type = 'output'


class OutputPlugin:
    def __init__(self, bot, channel):
        self.bot: Bot = bot
        self.channel: TextChannel = channel
        self.redis = RedisClient(prefix='output')

    async def process_response(self, response: Union[str, tuple]):
        if isinstance(response, (tuple, list)):
            return (await self._process_response(r) for r in response)
        return await self._process_response(response)

    async def _process_response(self, response: str):
        return response

    async def get_random_user(self, condition=None, bot=False) -> Member:
        if condition == 'prev':
            ruser_id = self.redis.get('lastruser')
            ruser = await self.bot.fetch_user(int(ruser_id))
            if ruser:
                return ruser

        acceptable = []
        member: Member
        for member in self.channel.members:
            member._user
            if member.id == self.bot.user.id:
                # that's me!
                continue

            if bot:
                # we're looking for another bot
                if not member.bot:
                    continue
            else:
                # we're looking for a user
                if member.bot:
                    continue

            if condition == 'male':
                if userinfo.get_user_gender(member) == userinfo.MALE:
                    acceptable.append(member)
            elif condition == 'female':
                if userinfo.get_user_gender(member) == userinfo.FEMALE:
                    acceptable.append(member)
            elif condition == 'like':
                if friendship.like(member):
                    acceptable.append(member)
            elif condition == 'dislike':
                if not friendship.like(member):
                    acceptable.append(member)
            elif condition == 'friend':
                if friendship.get_friendship(member) >= 50:
                    acceptable.append(member)
            elif condition == 'enemy':
                if friendship.get_friendship(member) < 50:
                    acceptable.append(member)
            else:
                acceptable.append(member)

        if acceptable:
            # we found some acceptable matches for our condition.
            # return a random one of those.
            ruser = random.choice(acceptable)
        else:
            # there we no acceptable matches. return a random user
            # from the channel as a fallback.
            if condition == '':
                ruser = random.choice(self.channel.members)
            else:
                return ''

        self.redis.set('lastruser', ruser.id)
        return ruser
