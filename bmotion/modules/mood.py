from discord.ext.commands import Bot

from helpers.messaging import send_message
from helpers.redis import RedisClient

MOODS = [
    'lonely',
    'happy',
    'horny',
    'stoned'
]


def redis() -> RedisClient:
    return RedisClient(prefix='module:mood')


async def frightened(channel, user):
    await send_message("%VAR{frightens} %SMILEY{sad}", channel, user)
    adjust_mood('lonely', -1)
    adjust_mood('happy', -1)


def get_happy(multiplier=1):
    adjust_mood('happy', 1 * multiplier)


def get_sad(multiplier=1):
    adjust_mood('happy', -1 * multiplier)


def get_horny(multiplier=1):
    adjust_mood('horny', 1 * multiplier)


def get_unhorny(multiplier=1):
    adjust_mood('horny', -1 * multiplier)


def get_lonely(multiplier=1):
    adjust_mood('lonely', 1 * multiplier)


def get_unlonely(multiplier=1):
    adjust_mood('lonely', -1 * multiplier)


def adjust_mood(mood, value):
    if value < 0:
        redis().decr(mood, value * -1)
    elif value > 0:
        redis().incr(mood, value)


def get_mood(mood):
    return redis().get(mood, 0)


def setup(bot: Bot):
    pass


def teardown(bot: Bot):
    pass
