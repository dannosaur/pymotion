import random

from discord.ext import commands
from discord.ext.commands import Bot, Context

from helpers.redis import RedisClient

PLUGIN_INIT = {}


def redis() -> RedisClient:
    return RedisClient(prefix='module:maisie')


def setup(bot: Bot):
    bot.add_command(maisie)
    bot.add_command(maisiemax)


@commands.command(name='maisie')
async def maisie(ctx: Context):
    await ctx.send(f'maisie{roll_dice()}')


@commands.command(name='maisiemax')
async def maisiemax(ctx, num: int = None):
    if not num:
        max_maisie = redis().get('max')
        await ctx.send(f"Maximum Maisie is currently {max_maisie}")
    else:
        redis().set('max', num)
        await ctx.send(f"Maximum Maisie is now {num}")


def roll_dice():
    max_maisie = redis().get('max', 1)
    last_10: list = redis().get('last_10', [])

    def _roll():
        num = random.randint(1, max_maisie)
        if num not in last_10:
            last_10.append(num)
            while len(last_10) > 10:
                last_10.pop(0)
            redis().set('last_10', last_10)
            return num
        return _roll()

    return _roll()
