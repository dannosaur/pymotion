import random
from datetime import datetime
import logging
import random
from datetime import datetime
from uuid import uuid4

import humanize
import pytz
from apscheduler.job import Job
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.interval import IntervalTrigger
from discord import TextChannel, PartialMessage
from discord.ext import commands
from discord.ext.commands import Bot, Context, Command
from snowflake import SnowflakeGenerator

from helpers.conf import Config
from helpers.messaging import owner_only
from helpers.redis import RedisClient

logger = logging.getLogger(__name__)

PLUGIN_INIT = {}


def redis() -> RedisClient:
    return RedisClient(prefix='module:scheduler')


class ScheduledContext(Context):
    def __init__(self, **kwargs):
        self._channel = kwargs.pop('channel', None)
        super().__init__(**kwargs)

    def _get_channel(self):
        pass


class CommandScheduler:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.scheduler = AsyncIOScheduler()
        jobstore = RedisJobStore(
            host=Config.getpath('redis.host'),
            port=Config.getpath('redis.port'),
            db=Config.getpath('scheduler.redis_db'),
        )
        self.scheduler.add_jobstore(jobstore)
        self.scheduler.start()
        self.config = redis().get('config', {})
        self._paused = redis().get('is_paused', False)

        if self._paused:
            logger.info("Scheduler restarted in paused mode")
            self.scheduler.pause()

        self.reload_config()

    def reload_config(self):
        logger.info("Reloading schedule config...")
        for command_id, command_config in self.config.items():
            job: Job = self.scheduler.get_job(command_id)
            if job:
                logger.info(f"Job with ID {command_id} exists, skipping")
                continue

            trigger = IntervalTrigger(
                seconds=int(command_config['interval']),
                jitter=int(command_config.get('jitter', 0)),
            )
            logger.info(f"Adding job of ID {command_id}")
            self.scheduler.add_job(
                'bmotion.modules.scheduler:command_callback',
                id=command_id,
                trigger=trigger,
                kwargs={
                    'command_id': command_id,
                }
            )

        for job in self.scheduler.get_jobs():
            if job.id not in self.config.keys():
                logger.info(f"Removing unconfigured job {job.id}")
                job.remove()

    async def add_job(self, ctx: Context, command: str = None, message: str = None, interval: int = None,
                      jitter: int = None):
        command_id = str(uuid4())

        if not command and not message:
            await ctx.send("Need either a command to invoke or message to send")
            return

        self.config[command_id] = {
            'command': command,
            'message': message,
            'interval': interval,
            'jitter': jitter or 0,
            'channel': {
                'id': ctx.channel.id,
            }
        }
        self.reload_config()
        await ctx.send(f"Job added with ID {command_id}")
        await self.save_config(ctx, output=False)

    async def list_jobs(self, ctx: Context, all_jobs: bool = False):
        messages = []
        for command_id, command_config in self.config.items():
            if not all_jobs and command_config['channel']['id'] != ctx.channel.id:
                continue

            job = self.scheduler.get_job(command_id)
            next_run_time = job.next_run_time.astimezone(pytz.UTC) - datetime.now().astimezone(pytz.UTC)
            content = [
                f"Command ID: {command_id}",
                f"Command: {command_config['command']}" if command_config[
                    'command'] else f"Message: {command_config['message']}",
                f"Delay: {command_config['interval']}",
                f"Jitter: {command_config['jitter']}",
                f"Next Run: {job.next_run_time} (in {humanize.precisedelta(next_run_time, format='%0.0f')})",
                f"Channel ID: {command_config['channel']['id']}",
            ]
            messages.append(" | ".join(content))

        if not messages:
            if not all_jobs:
                await ctx.send("No jobs scheduled in this channel")
            else:
                await ctx.send("No jobs scheduled")
            return

        output = "\n".join(messages)
        print(output)
        await ctx.send(f"```{output}```")

    async def delete_job(self, ctx: Context, command_id: str):
        command_config = self.config.get(command_id)
        if not command_config:
            await ctx.send(f"Command with ID {command_id} not found")
            return

        job = self.scheduler.get_job(command_id)
        job.remove()
        del self.config[command_id]
        await ctx.send(f"Job {command_id} deleted")
        await self.save_config(ctx, output=False)

    async def save_config(self, ctx: Context = None, output=True):
        r = redis()
        r.set('config', self.config)
        r.set('is_paused', self._paused)
        logger.info("Config saved")
        if output and ctx:
            await ctx.send("Scheduler config saved")

    def shutdown(self):
        logger.info("Shutting down scheduler")
        self.scheduler.shutdown()

    async def pause(self, ctx: Context):
        self.scheduler.pause()
        self._paused = True
        await self.save_config(output=False)
        await ctx.send("Scheduler paused")

    async def resume(self, ctx: Context):
        self.scheduler.resume()
        self._paused = False
        await self.save_config(output=False)
        await ctx.send("Scheduler resumed")


def setup(bot: Bot):
    bot.add_command(scheduler_root)
    scheduler = CommandScheduler(bot)
    PLUGIN_INIT['scheduler'] = scheduler


def teardown(bot: Bot):
    PLUGIN_INIT['scheduler'].shutdown()


async def command_callback(command_id):
    scheduler = PLUGIN_INIT['scheduler']
    logger.info(f"Triggering callback for {command_id}...")
    command_config = scheduler.config[command_id]

    channel: TextChannel = scheduler.bot.get_channel(command_config['channel']['id'])

    generator = SnowflakeGenerator(random.randint(0, 1023))
    message = PartialMessage(
        channel=channel,
        id=next(generator),
    )
    context = Context(
        message=message,
        bot=scheduler.bot,
        prefix=Config.get('cmd_prefix'),
    )

    if command_config['command']:
        command: Command = scheduler.bot.get_command(name=command_config['command'])
        await context.invoke(command)
    elif command_config['message']:
        await channel.send(command_config['message'])


@commands.command(name='scheduler')
@owner_only()
async def scheduler_root(ctx, cmd: str, *params):
    scheduler = PLUGIN_INIT['scheduler']

    try:
        fn = getattr(scheduler, cmd)
    except AttributeError:
        await ctx.send(f"Command {cmd} not found")
        return

    args = []
    kwargs = {}
    for param in params:
        try:
            key, value = param.split('=')
        except ValueError:
            args.append(param)
        else:
            kwargs[key] = value

    try:
        await fn(ctx, *args, **kwargs)
    # except TypeError as e:
    #     await ctx.send(f"Error invoking {cmd}: {str(e)}")
    except Exception as e:
        await ctx.send(f"Error invoking {cmd}: {str(e)}")
