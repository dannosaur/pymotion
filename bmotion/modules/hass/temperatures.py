import os
from io import BytesIO

import discord
import requests
from discord.ext import commands
from discord.ext.commands import Bot, Context
from homeassistant_api import Client

from helpers.conf import Config

HASS_API = os.environ.get('HASS_API')
HASS_AUTH = os.environ.get('HASS_AUTH')


@commands.command(name='hometemp')
async def command_handler(ctx: Context):
    try:
        hass = Client(HASS_API, HASS_AUTH)
    except:
        await ctx.send("Error communicating with HomeAssistant")
        return

    office_temp = hass.get_entity(entity_id=Config.getpath('temperatures.hass_entities.office_temp')).get_state()
    office_humidity = hass.get_entity(entity_id=Config.getpath('temperatures.hass_entities.office_humidity')).get_state()

    outside_temp = hass.get_entity(entity_id=Config.getpath('temperatures.hass_entities.outside_temp')).get_state()
    outside_humidity = hass.get_entity(entity_id=Config.getpath('temperatures.hass_entities.outside_humidity')).get_state()
    outside_pressure = hass.get_entity(entity_id=Config.getpath('temperatures.hass_entities.outside_pressure')).get_state()

    await ctx.send(
        f"Office: {round((float(office_temp.state) - 32) * 5 / 9, 1)}°C {office_humidity.state}% RH | "
        f"Outside: {round((float(outside_temp.state) - 32) * 5 / 9, 1)}°C {outside_humidity.state}% RH {outside_pressure.state} hPa"
    )


def setup(bot: Bot):
    bot.add_command(command_handler)
