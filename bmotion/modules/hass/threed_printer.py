import os
from io import BytesIO

import discord
import requests
from discord.ext import commands
from discord.ext.commands import Bot, Context
from homeassistant_api import Client

from helpers.conf import Config

HASS_API = os.environ.get('HASS_API')
HASS_AUTH = os.environ.get('HASS_AUTH')


@commands.command(name='printer')
async def printer_command_handler(ctx: Context, show_image: bool = False):
    try:
        hass = Client(HASS_API, HASS_AUTH)
    except:
        await ctx.send("Error communicating with HomeAssistant")
        return

    printing = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.printing')).get_state()
    if printing.state != 'on':
        await ctx.send("Not printing anything")
        return

    filename = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.filename')).get_state()
    environment_temp = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.environment')).get_state()
    print_progress = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.print_progress')).get_state()
    hotend_temp = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.hotend')).get_state()
    bed_temp = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.bed')).get_state()
    print_duration = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.print_duration')).get_state()
    print_eta = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.print_eta')).get_state()
    printer_power = hass.get_entity(entity_id=Config.getpath('threed_printer.hass_entities.printer_power')).get_state()

    message_kwargs = {}
    if ctx.message.author.id == Config.get('owner') and show_image:
        camera_image = requests.get(
            url=f'{HASS_API}/camera_proxy/{Config.getpath("threed_printer.hass_entities.camera")}',
            headers={
                'Authorization': f'Bearer {HASS_AUTH}',
            },
            params={
                'width': 640,
                'height': 480
            }
        )
        message_kwargs['file'] = discord.File(BytesIO(camera_image.content), filename='image.jpg')

    lines = [
        f"Printing: {filename.state}",
        f"Print progress: {print_progress.state}% | "
        f"Hotend temp: {hotend_temp.state}°C | "
        f"Bed temp: {bed_temp.state}°C | "
        f"Environment temp: {environment_temp.state}°C | "
        f"Power: {printer_power.state}W | "
        f"Duration: {print_duration.state} | "
        f"ETA: {print_eta.state}"
    ]
    await ctx.send(
        '\n'.join(lines),
        **message_kwargs
    )


def setup(bot: Bot):
    bot.add_command(printer_command_handler)
