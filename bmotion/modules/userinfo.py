import random

from discord import User
from discord.ext import commands
from discord.ext.commands import Bot, Context

from helpers.redis import RedisClient

UNKNOWN = 'unknown'
MALE = 'male'
FEMALE = 'female'
GENDERS = [MALE, FEMALE]
PLUGIN_INIT = {}


def redis() -> RedisClient:
    return RedisClient(prefix='module:userinfo')


def get_user_gender(user: User):
    return redis().get(f'{user.id}:gender', UNKNOWN)


def set_user_gender(user: User, gender):
    redis().set(f'{user.id}:gender', gender)


def get_user_real_name(user: User, choose=False):
    real_names: list = redis().get(f'{user.id}:realnames', [])
    if not real_names:
        return user.mention

    real_names += [user.mention, user.display_name]
    if choose:
        return random.choice(real_names)
    else:
        return real_names


def add_user_real_name(user: User, name: str):
    real_names: list = redis().get(f'{user.id}:realnames', [])
    real_names.append(name)
    redis().set(f'{user.id}:realnames', real_names)


@commands.command(name='gender')
async def set_user_gender_command(ctx: Context, arg=None):
    if not arg:
        gender = get_user_gender(ctx.author)
        await ctx.send(f"{ctx.author.mention}'s gender is **{gender}**")
    elif arg.lower() not in GENDERS:
        await ctx.send(f"**{arg}** is not a valid gender")
    else:
        set_user_gender(ctx.author, arg)
        await ctx.send(f"{ctx.author.mention}'s gender is now **{arg}**")


def setup(bot: Bot):
    bot.add_command(set_user_gender_command)


def teardown(bot: Bot):
    pass
