from typing import Union

from discord import User, Member
from discord.ext import commands
from discord.ext.commands import Context, Bot

from bmotion.modules import userinfo
from bmotion.modules.mood import get_mood
from helpers.conf import Config
from helpers.messaging import owner_only
from helpers.redis import RedisClient


def redis() -> RedisClient:
    return RedisClient(prefix='module:friendship')


def drift_friendship(user: User, value):
    if value < 0:
        redis().decr(user.id, value)
    elif value > 0:
        redis().incr(user.id, value)


def get_friendship(user: User):
    return redis().get(user.id, 0)


@commands.command(name='friendship')
@owner_only()
async def get_user_friendship_command(ctx: Context, user: Union[User, None] = None):
    if user:
        points = get_friendship(user)
        await ctx.send(f"{user.mention}'s friendship points is currently {points}")
    else:
        points = get_friendship(ctx.author)
        await ctx.send(f"{ctx.author.mention}'s friendship points is currently {points}")


@commands.command(name='like')
@owner_only()
async def get_like(ctx, member: Union[Member, str]):
    if not isinstance(member, Member):
        await ctx.send(f"Tag someone.")
        return

    like_status = like(member)
    if not like_status:
        await ctx.send(f"I don't like {member.mention}")
    else:
        await ctx.send(f"I quite like {member.mention}")


def like(user=None):
    if Config.get('melmode', False):
        # "melmode" is on. i'll do anyone
        return True

    if not user:
        if get_mood('stoned') > 20:
            # i'm sufficiently stoned. i'll do anyone
            return True

        if get_mood('horny') > 10:
            # i'm horny. i'll do anyone
            return True

        # get lost
        return False

    if get_friendship(user) < 40:
        # i don't do people i'm not friends with
        return False

    # we know the user, and we're friends. let's work out their gender
    gender = userinfo.get_user_gender(user)
    if not gender:
        # user is genderless, so I'll do them. it's only 50/50 anyway. i like those odds!
        return True

    if gender == Config.get('gender'):
        # they're my gender
        if Config.get('orientation') in ['bi', 'gay', 'lesbian']:
            return True
        return False

    # they're not my gender. now what?
    if Config.get('orientation') in ['bi', 'straight']:
        # i'd still do 'em..
        return True

    # that only leaves lesbian and gay who won't sleep with the opposite gender
    return False


def setup(bot: Bot):
    bot.add_command(get_user_friendship_command)
    bot.add_command(get_like)


def teardown(bot: Bot):
    pass
