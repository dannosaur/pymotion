import logging
import re

from discord.ext import commands
from discord.ext.commands import Context, Bot

from helpers.messaging import owner_only, send_message
from helpers.redis import RedisClient

logger = logging.getLogger('modules.system')

PLUGIN_INIT = {}

IGNORES = []


def bot() -> Bot:
    return PLUGIN_INIT['bot']


def redis() -> RedisClient:
    return RedisClient(prefix=RedisClient.NO_PREFIX)


async def do_ignore(ctx: Context, subcmd, user=None):
    logger.debug(f"do_ignore: {subcmd} - {user}")
    redis_key = 'system:ignores'
    if subcmd == 'add':
        if item_matches := re.match(r'<@!?(?P<user_id>\d+)>', user):
            ignores = redis().get(redis_key, [])
            ignores.append(int(item_matches.group('user_id')))
            IGNORES.append(int(item_matches.group('user_id')))
            redis().set(redis_key, ignores)
            await send_message(f"Added ignore for {user}", ctx.message.channel, use_timers=False)
            return

    if subcmd == 'remove':
        if item_matches := re.match(r'<@!?(?P<user_id>\d+)>', user):
            ignores: list = redis().get(redis_key, [])
            ignores.remove(int(item_matches.group('user_id')))
            IGNORES.remove(int(item_matches.group('user_id')))
            redis().set(redis_key, ignores)
            await send_message(f"Removed ignore for {user}", ctx.message.channel, use_timers=False)
            return

    if subcmd == 'list':
        ignores: list = redis().get(redis_key, [])
        ignore_members = [await bot().fetch_user(i) for i in ignores]
        await send_message(f"I'm ignoring the following: {', '.join(m.mention for m in ignore_members)}", ctx.message.channel, use_timers=False)


async def do_redis(ctx: Context, key):
    value = redis().get(key, None)
    await send_message(f'{key} = {value}', ctx.message.channel, use_timers=False)
    return


@commands.command(name='system')
@owner_only()
async def system_entry_command(ctx: Context, *args):
    try:
        cmd = args[0]
    except IndexError:
        await send_message("You need to give me a command, dumbass.", ctx.message.channel, use_timers=False)
        return

    if cmd == 'redis':
        await do_redis(ctx, args[1])
    elif cmd == 'ignore':
        await do_ignore(ctx, *args[1:])


def setup(bot: Bot):
    bot.add_command(system_entry_command)
    PLUGIN_INIT['bot'] = bot

    logger.info("Loading ignores..")
    ignores = redis().get('system:ignores', [])
    IGNORES.extend(ignores)


def teardown(bot: Bot):
    pass
