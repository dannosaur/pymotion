import glob
import os
import random

from discord.ext.commands import Bot

from bmotion.plugins import Plugin
from helpers.redis import RedisClient

PLUGIN_INIT = {}

ABSTRACTS = {
    "permission_denied": [
        "You're not my daddy",
        "...",
        "the fuck are you?",
        "My secrets are between me, and my owner.",
        "_yawn_",
    ],
    "go on then": ["sssh sekrit", "go on then", "oh go on then", "ok then, but don't tell anyone"],
    "no's": ["no.", "no", "No.", "No", "certainly not", "don't be so silly", "nope", "negative", "nup", "nada", "nein",
             "no siree bob", "maybe where you come from", "yesyesyesyesyesyes%|no", "nnnk", "/shakes head",
             "*shakes head*"],
    "rarrs": ["~rarr~", "~oof~", "uNf", "*uNf", "*squeaky*", "*boing*", "%REPEAT{3:10:bl}", "*spangle*",
              "~oef~"],
    "awwws": ["awww", "awww%|poor %%", "awww%|/kisses it better", "awww%|/rubs %% better", "awww%|/sniggers at %%"],
    "ididnt": ["no, *I* didn't", "Oh really.", "Yes you did. We all saw it.", "Of course you didn't.",
               "Oh yes you did.", "You must think us all fools.", "nnk", "We all saw you", "I have proof you did",
               "caught on camera!"],
    "bored": [
        "aww%|/hugs %%",
        "/tickles %%", "cheer up %%%|/hugs",
        "/feels %%",
        "/gets out a board game for %%",
        "/starts shuffling cards",
        "/befriends %%",
        "/downloads some porn for %%",
        "aww%|what's the matter, actual-size %%?",
    ],
    "dude": [
        "Dude!",
        "My god dude!",
        "D%REPEAT{5:10:u}de!",
    ],
    "sweet": [
        "Sweet!",
        "Schweet!",
        "Sw%REPEAT{5:10:e}t!",
    ],
    "prom": ["%VAR{prom_first}-$VAR{prom_second}"],
    "frightens": [
        "eek!",  # %|%bot\[50,�VAR{awwws}\]"
        "o_O",
        "erk",
        "bah",
        "oh no b-",
        "crikey",  # %|%bot\[50,�VAR{awwws}\]"
        "blimey",  # %bot\[50,�VAR{awwws}\]"
        "gosh",
        "crumbs",  # %|%bot\[50,�VAR{awwws}\]"
        "yof",
        "ohmigod!",
        "erp!",
    ],
    "colours": [
        "%VAR{basic_colours}",
        "%VAR{weird_colours}",
        "%VAR{colour_adjective} %VAR{basic_colours}",
        "%VAR{colour_adjective} %VAR{weird_colours}",
    ],
    "satons": ["hey out %SMILEY{sad}", "heeeyyy :O", "bah", "arrrrgh", "erk", "gerrof!", "NNK", "mmmph!"],
    "sfx": [
        "%VAR{sounds}",
        "*%VAR{sounds}*",
        "(%VAR{sounds})",
        "%VAR{sounds}!",
    ],
    "sounds": [
        "kyaaah",
        "squelch",
        "poke",
        "ph%REPEAT{3:7:b}t",
        "FLARPT",
        "neigh",
        "ffworpp",
        "gwaab",
        "ffffflout",
        "arooga",
        "click-click!",
        "klackety",
        "feep-feep",
        "*eeeem*",
        "honk honk!",
        "uh-uh-uh",
        "whommm",
        "eep",
        "glop",
        "splish-splash-woah",
        "FOOM",
        "CLACK",
        "hiccup",
        "hee-haw",
        "splatter",
        "slap slap",
        "arrg splutter",
        "aww",
        "*kaw*",
        "ZAP",
        "fweeee",
        "sploosh",
        "snip!",
        "pap",
        "*choo choo choo*",
        "chuff!",
        "slip-beeeeeee",
        "smack",
        "oook-oook",
        "gah",
        "gibber",
        "goo",
        "harrumph",
        "whip",
        "bzz-bzz-bzz-bzz",
        "splutter",
        "tweet tweet",
        "ock",
        "wobble wobble!",
        "slash!",
        "ulp-schwip",
        "drip",
        "splish",
        "cough",
        "wobble",
        "blaff",
        "flick",
        "slep",
        "rustle",
        "whack",
        "arf",
        "voom",
        "yuck",
        "tsk",
        "tick",
        "noooo",
        "hop",
        "bep",
        "dong-doof",
        "whump",
    ],
    "yays": [
        "yay",
        "woo",
        "whee",
        "hooray",
        "winnar",
        "best",
        "har",
    ],
    "bodypart": [
        "brain",
        "buttock",
        "arse",
        "lip",
        "mouth flaps",
        "eyelash",
        "toe",
        "foot",
        "ankle",
        "leg",
        "knee",
        "groin",
        "bum",
        "stomach",
        "chest",
        "back",
        "throat",
        "arm",
        "hand",
        "finger",
        "thumb",
        "nail",
        "head",
        "ear",
        "nose",
        "nostril",
        "eyeball",
        "tooth",
        "tongue",
        "mouth",
        "buns",
        "shin",
        "solar plexus",
        "kidney",
        "hair",
        "wrist",
        "eye",
        "eyebrow",
        "shoulder",
        "elbow",
        "knuckle",
        "index finger",
        "middle finger",
        "ring finger",
        "little finger",
        "spine",
        "thigh",
        "ankle",
        "big toe",
        "little toe",
        "toe",
        "thigh",
        "spleen",
        "wrist",
        "forearm",
        "shoulder",
        "thigh",
        "neck",
        "heart",
    ],
    "bodypart_male": [
        "cock",
        "testicle",
        "manboob",
        "scrotum",
        "ballbag",
        "dicksack",
    ],
    "bodypart_female": [
        "haddock flaps",
        "front lumps",
        "vagina",
        "fanny",
        "front bottom",
        "minge",
        "breast",
        "boob",
        "wab",
        "nipple",
        "labia",
    ],
    "sound": [
        "%VAR{sounds} %VAR{sounds} %VAR{sounds} %VAR{sounds} %VAR{sounds} %VAR{sounds}",
        "%VAR{sounds} %VAR{sounds} %VAR{sounds} %|oh! excuse me",
        "%VAR{sounds} %VAR{sounds}%|/giggles",
        "%VAR{sounds} %VAR{sounds}",
    ],
    "sound_short": [
        "%VAR{sfx} %VAR{sfx} %VAR{sfx} %VAR{sound2}",
    ],
    "sound_answer": [
        "%VAR{sound_short}",
        "a bit like %VAR{sound_short}",
        "kinda %VAR{sound_short}",
        "%VAR{sound_short}, ish",
    ],
    "sound2": [
        "%VAR{sounds} %VAR{sound2} ",
        "%VAR{sfx} %VAR{sound2} ",
        "%VAR{sounds} ",
        "%VAR{sfx} ",
    ],
    "goAways": [
        "go away",
        "piss off",
        "shut up",
        "get lost",
        "...",
        "make like a banana",
        "sod off",
        "bugger off",
    ],
}


def redis() -> RedisClient:
    return PLUGIN_INIT['redis']


def load_abstracts():
    for txt in glob.glob(os.path.join(os.path.dirname(__file__), 'abstracts', '*.txt')):
        filename = os.path.basename(txt)
        abstract_name, _ = filename.split('.')
        print(f"Loading abstracts: {abstract_name}")
        abstracts = []
        with open(txt, 'r') as f:
            for line in f.read().split("\n"):
                if line.strip() == '':
                    continue
                elif line.startswith('#'):
                    continue
                elif line.startswith('SS#'):
                    params = line.split('#')
                    abstracts.append(tuple(params[1:]))
                else:
                    abstracts.append(line)
            ABSTRACTS[abstract_name] = abstracts

    for key in ABSTRACTS.keys():
        dynamic_abstracts = redis().get(key, [])
        if dynamic_abstracts:
            print(f"Loading dynamic abstracts: {key}")
            ABSTRACTS[key].extend(dynamic_abstracts)


def register_abstract(name, lst):
    ABSTRACTS[name] = lst


def add_to_abstract(name, item):
    if item not in ABSTRACTS[name]:
        abstract_list = redis().get(name, [])
        abstract_list.append(item)
        redis().set(name, abstract_list)


def get_abstract(name):
    if name not in ABSTRACTS or len(ABSTRACTS[name]) == 0:
        return f"Internal Error: no responses to choose from (key == {name})"

    item = random.choice(ABSTRACTS[name])
    redis().set('last', item)
    return item


def setup(bot: Bot):
    PLUGIN_INIT['redis'] = RedisClient(prefix='module:abstracts')
    load_abstracts()


def teardown(bot: Bot):
    pass
