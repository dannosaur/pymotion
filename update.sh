#!/bin/bash

echo "Stopping service..."
service pymotion stop

echo "Updating code.."
git pull

echo "Updating requirements..."
venv/bin/pip install -r requirements.txt

echo "Restarting service..."
service pymotion start

echo "Done."
