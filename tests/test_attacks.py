import unittest
from unittest import TestCase

from bmotion.plugins.complex.attacks import immolation_tokenation


class TestAttacks(TestCase):
    def test_tokenization_simple_and(self):
        self.assertEqual(['doug', 'woo'], immolation_tokenation("doug and woo"))

    def test_tokenization_discord_tag_variant_1(self):
        self.assertEqual(["<@12345>"], immolation_tokenation("<@12345>"))

    def test_tokenization_discord_tag_variant_2(self):
        self.assertEqual(["<@!12345>"], immolation_tokenation("<@!12345>"))

    def test_tokenization_multiple_discord_tags(self):
        self.assertEqual(["<@12345>", "<@23456>"], immolation_tokenation("<@12345> and <@23456>"))

    def test_tokenization_multiple_discord_tags_commas(self):
        self.assertEqual(["<@12345>", "<@23456>"], immolation_tokenation("<@12345>, and <@23456>"))

    def test_tokenization_multiple_discord_tag_apostrophe(self):
        self.assertEqual(["<@12345>'s penchant for fire"], immolation_tokenation("<@12345>'s penchant for fire"))

    def test_multiple_tokens_improper_english(self):
        self.assertEqual(['doug', 'woo', 'gary'], immolation_tokenation("doug, woo and gary"))

    def test_multiple_tokens_proper_english(self):
        self.assertEqual(['doug', 'woo', 'gary'], immolation_tokenation("doug, woo, and gary"))

    def test_words_with_apostrophes(self):
        self.assertEqual(["doug's computer"], immolation_tokenation("doug's computer"))

    def test_multiple_items_with_apostrophes(self):
        self.assertEqual(["doug's computer", "woo's guitar"], immolation_tokenation("doug's computer and woo's guitar"))


if __name__ == '__main__':
    unittest.main()
