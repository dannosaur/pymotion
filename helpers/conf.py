import logging
import logging.config
import os
import pathlib
from pprint import pprint

import yaml
from discord import Intents
from discord.ext.commands import Bot


LOGGING = {
    'version': 1,
    'filters': {},
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'main_formatter',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'DEBUG',
            'formatter': 'main_formatter',
            'filename': os.environ.get('LOG_PATH', '/var/log/pymotion.log'),
            'mode': 'a',
            'maxBytes': 10485760,
            'backupCount': 5,
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '[%(asctime)s] - [%(levelname)s] - [%(name)s] : %(message)s'
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['console', 'file'],
    },
    'loggers': {
        'discord': {
            'level': 'ERROR',
            'handlers': ['console', 'file'],
        }
    }
}


class ConfigError(Exception):
    pass


class _Config(dict):
    def __init__(self):
        filename = os.environ.get('CONFIG_FILE', 'config.yml')
        base_dir = pathlib.Path(__file__).parent.parent.resolve()
        print(f"loading {base_dir}/{filename}")

        with open(os.path.join(base_dir, filename), 'r') as f:
            bot_config = yaml.safe_load(f)

        super().__init__(**bot_config['bot'])

    def getpath(self, path):
        pieces = path.split('.')
        d = self
        for piece in pieces:
            d = d[piece]
        return d


Config = _Config()


def load_extensions(bot: Bot):
    extensions_config = Config.get('extensions', [])
    for extension in extensions_config:
        print(f"loading extention: {extension}")
        bot.load_extension(extension)


def load_cogs(bot: Bot):
    from bmotion.plugins import Plugin
    Plugin.set_bot(bot)

    from bmotion.plugins.simple import SimplePluginCog
    bot.add_cog(SimplePluginCog(bot))

    from bmotion.plugins.complex import ComplexPluginCog
    bot.add_cog(ComplexPluginCog(bot))

    from bmotion.plugins.output import OutputPluginCog
    bot.add_cog(OutputPluginCog(bot))


def configure_logging():
    # logging.basicConfig(level=logging.DEBUG)
    logging.config.dictConfig(LOGGING)


def get_intents():
    intents = Intents.default()
    intents.members = True
    return intents

