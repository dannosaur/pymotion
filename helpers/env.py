import os
import pathlib


def load_env_file(filename='.environment'):
    base_dir = pathlib.Path(__file__).parent.parent.resolve()

    with open(os.path.join(base_dir, filename), 'r') as f:
        env = f.read()

    for line in env.split('\n'):
        line = line.strip()
        if not line:
            continue

        if line.startswith('#'):
            continue

        key, value = line.split('=')

        os.environ.setdefault(key, value)
