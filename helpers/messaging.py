import asyncio
import logging
import random
import re
from typing import Union, List

from discord import User, TextChannel, Embed
from discord.ext import commands

from bmotion.modules.userinfo import get_user_real_name
from bmotion.plugins import Plugin
from helpers.conf import Config
from helpers.human import sleep_proportional_to_response

format_message_logger = logging.getLogger('helpers.messaging.format_message')
send_message_logger = logging.getLogger('helpers.messaging.send_message')


def owner_only():
    def predicate(ctx):
        return ctx.message.author.id == Config.get('owner')

    return commands.check(predicate)


async def format_response(
    response_msg: str,
    channel: TextChannel,
    user: Union[User, None] = None,
    *interpolation_args,
    **params
) -> List[str]:
    format_message_logger.debug(f"In: {response_msg}")
    _response_msg = str(response_msg)
    _response_msg = await Plugin.process_output(response_msg, channel)

    system_params = {
        'cmd_prefix': Config.get('cmd_prefix'),
    }
    user_params = {}
    if user:
        user_params = {
            # TODO is this one used?!
            # 'name': user.name,
            'user_tag': user.mention,
            'user': get_user_real_name(user, choose=True),
        }

    _response_msg = _response_msg.format(
        **system_params,
        **user_params,
        **params
    )

    format_message_logger.debug(f"Interpolation args: {interpolation_args}")
    if interpolation_args:
        _response_msg = re.sub(r'%%', interpolation_args[0], _response_msg)
        _response_msg = re.sub(r'%(\d+)', lambda m: interpolation_args[int(m.group(1)) - 1], _response_msg)

    _response_msg = _response_msg.split('%|')
    for idx, _piece in enumerate(_response_msg):
        format_message_logger.debug(f"Piece In: {_piece}")
        if _piece.startswith('/'):
            _response_msg[idx] = f"_{_piece[1:]}_"
        format_message_logger.debug(f"Piece Out: {_response_msg[idx]}")

    format_message_logger.debug(f"Out: {_response_msg}")
    return _response_msg


async def send_message(
    response: Union[str, list, tuple, Embed],
    channel: TextChannel,
    user: Union[User, None] = None,
    params: Union[dict, None] = None,
    interpolation_args: List[str] = None,
    use_timers: bool = True,
):
    send_message_logger.debug(f"Sending: {response}")
    params = params or {}
    interpolation_args = interpolation_args or [user.mention] if user else []
    if use_timers:
        await asyncio.sleep(random.randint(1, 3))

    if not isinstance(response, Embed):
        final_response = await format_response(response, channel, user, *interpolation_args, **params)
    else:
        final_response = response

    if isinstance(final_response, (list, tuple)):
        if use_timers:
            async with channel.typing():
                for r in final_response:
                    await asyncio.sleep(sleep_proportional_to_response(r))
                    await channel.send(r)
        else:
            for r in final_response:
                await channel.send(r)
    elif isinstance(final_response, Embed):
        await channel.send(embed=final_response)
