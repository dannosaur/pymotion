import json
import pickle
import zlib

from redis import Redis

from helpers.conf import Config


class PickleSerializer:
    pickle_version = 1

    def serialize(self, data):
        return pickle.dumps(data, self.pickle_version)

    def unserialize(self, data):
        return pickle.loads(data)


class JsonSerializer:
    def serialize(self, data):
        return json.dumps(data)

    def unserialize(self, data):
        return json.loads(data)


class ZLibCompressor:
    level = 6

    def compress(self, data):
        return zlib.compress(data, level=self.level)

    def decompress(self, data):
        return zlib.decompress(data)


class NoopCompressor:
    def compress(self, data):
        return data

    def decompress(self, data):
        return data


Compressor = NoopCompressor
Serializer = JsonSerializer


class RedisClient:
    NO_PREFIX = object()

    def __init__(self, prefix):
        self.prefix = prefix

        self.compressor = Compressor()
        self.serializer = Serializer()

        redis = Config.get('redis')
        self.client = Redis(host=redis['host'], port=redis['port'], db=redis['db'])

    def get_key(self, key):
        if not isinstance(key, str):
            key = str(key)
        if self.prefix == RedisClient.NO_PREFIX:
            return key
        return ':'.join([self.prefix, key])

    def prep_value(self, value):
        value = self.serializer.serialize(value)
        value = self.compressor.compress(value)
        return value

    def to_python(self, value):
        value = self.compressor.decompress(value)
        value = self.serializer.unserialize(value)
        return value

    def get(self, key, default=None):
        value = self.client.get(self.get_key(key))
        if value is None:
            return default
        return self.to_python(value)

    def set(self, key, value):
        value = self.prep_value(value)
        return self.client.set(self.get_key(key), value)

    def delete(self, key):
        return self.client.delete(key)

    def incr(self, key, value=1):
        return self.client.incr(self.get_key(key), value)

    def decr(self, key, value=1):
        return self.client.decr(self.get_key(key), value)
