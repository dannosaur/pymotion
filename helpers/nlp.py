import spacy

nlp = spacy.load('en_core_web_sm')

verb_map = {
    'got': 'get',
    'sit': 'sat',
    'drink': 'drunk',
    'catch': 'caught',
    'bring': 'brought',
    'teach': 'taught',
    'have': 'had',
    'do': 'did',
    'ride': 'rode',
    'go': 'went',
    'make': 'made',
}


def make_past(token):
    return verb_map.get(token.text, token.lemma_ + 'ed')


spacy.tokens.Token.set_extension('make_past', getter=make_past, force=True)
