import os

import nltk
import spacy
from discord.ext.commands import Bot
from spacy.cli import download

from helpers.conf import load_extensions, load_cogs, Config, configure_logging, get_intents
from helpers.env import load_env_file

# sentry_sdk.init(
#     "https://ac3231ebd15847c5ae8b10443917e8a3@o193012.ingest.sentry.io/6000849",
#     environment=Config.get('name'),
#     # Set traces_sample_rate to 1.0 to capture 100%
#     # of transactions for performance monitoring.
#     # We recommend adjusting this value in production.
#     traces_sample_rate=1.0
# )

load_env_file()


def main():
    nltk.download('punkt')
    nltk.download('wordnet')
    nltk.download('averaged_perceptron_tagger')

    download('en_core_web_sm')

    configure_logging()
    bot = Bot(command_prefix=Config.get('cmd_prefix'), intents=get_intents())
    load_extensions(bot)
    load_cogs(bot)
    bot.run(os.environ.get('DISCORD_TOKEN'))


if __name__ == '__main__':
    main()
