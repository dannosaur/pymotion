import re
import sys
from pprint import pprint
from textblob import TextBlob
import boto3

comprehend = boto3.client('comprehend')


def get_targets(msg):
    response = comprehend.detect_key_phrases(
        Text=msg,
        LanguageCode='en'
    )
    pprint(response)

    return
    blob = TextBlob(msg)
    targets = []
    for i, (word, tag) in enumerate(blob.pos_tags):
        print(word, "=", tag)
        if tag == 'NN':
            try:
                next_word, next_tag = blob.pos_tags[i + 1]
                if next_tag == 'POS':
                    blob.pos_tags[i + 1] = (word + next_word, next_tag)
                    continue
            except IndexError:
                pass

            # check the previous tag
            prev_word, prev_tag = blob.pos_tags[i - 1]
            match prev_tag:
                case 'DT' | 'POS' | 'JJ':
                    targets.append(f'{prev_word} {word}')
                case 'PRPS':
                    targets.append(f"user's {word}")
                case _:
                    targets.append(word)

    return targets


tests = [
    'Woo482 sets his penis on fire',
    "Woo482 sets doug's penis on fire",
    "dan sets the stupid country on fire",
    # '_sets  and other thing on fire_',
    # '_sets layton, new york, and lexington ky on fire_',
    # '_sets his thing on fire_'
]

for test in tests:
    print(f"Testing: {test}")
    # matches = re.match(r'^_sets (.*) on fire(.*)_$', test)
    # pprint(matches.groups())
    targets = get_targets(test)
    # pprint(targets)
    # print("Test complete")
# for i, test in enumerate(tests):
#     for j, r in enumerate(regex):
#         if matches := re.match(r, test):
#             print(i, "=", j)
#             pprint(matches.group())
#             # break

